
public class Tondeuse extends Entite{
	
	private int ID;
	
	private int degats;
	
	private static final double TRUC_MOVE_X = 0.00552;
	private static final double TRUC_SIZE = 0.075;
	private boolean statut;
	
	private boolean bouge;
	
	public Tondeuse(double x, double y, int id) {
		super(x,y);
		this.ID = id;
		
		this.degats = 9999;
		
		this.statut = true;
		
		this.bouge = false;
	}
	
	public int getID() {
		return ID;
	}



	public void setID(int iD) {
		ID = iD;
	}
	
	public int getDegats() {
		return degats;
	}

	public void setDegats(int degats) {
		this.degats = degats;
	}

	@Override
	public void step() {
		
		if (this.position.getX() > 0.76) {
			statut = false; 
		}
		
		if(GameWorld.grille.collisionZombie(this.position.getX(), this.position.getY())) {
			for(double i=0.06;i<0.75;i+=0.07777) {
				int z=0;
				while(GameWorld.grille.getZombie(i, this.position.getY(), z) != null) {
					if(GameWorld.grille.getZombie(i, this.position.getY(), z) instanceof ZombieBase) {
						if(GameWorld.grille.getZombie(i, this.position.getY(), z).getX() < getX()){
							((ZombieBase) GameWorld.entites.get(GameWorld.grille.getZombie(i, this.position.getY(), z).getID())).setVie(((ZombieBase) GameWorld.entites.get(GameWorld.grille.getZombie(i, this.position.getY(), z).getID())).getVie() - getDegats());
						}
					}
					if(GameWorld.grille.getZombie(i, this.position.getY(), z) instanceof ZombieBlinde) {
						if(GameWorld.grille.getZombie(i, this.position.getY(), z).getX() < getX()){
							((ZombieBlinde) GameWorld.entites.get(GameWorld.grille.getZombie(i, this.position.getY(), z).getID())).setVie(((ZombieBlinde) GameWorld.entites.get(GameWorld.grille.getZombie(i, this.position.getY(), z).getID())).getVie() - getDegats());
						}
					}
					z++;
				}
			}
			bouge = true;
		}
		
		if(bouge) {
			this.position.setX(this.position.getX() + TRUC_MOVE_X);
		}
		
		if (!statut) {
			GameWorld.entites.set(getID(), null);
		}
	}

	@Override
	public void dessine() {
		if (statut) {
			StdDraw.picture(this.position.getX()-0.025, this.position.getY()-0.025, "image/tondeuse.GIF", TRUC_SIZE, TRUC_SIZE);
		}
	}

	@Override
	public String toString() {
		return "Soleil [id=" + getID() + "]";
	}
}
