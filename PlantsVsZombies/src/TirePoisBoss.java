
public class TirePoisBoss extends Plante{
	private Timer rechargeFeu;
	private Timer rechargeGele;
	private int vie;
	
	private static final double TRUC_SIZE = 0.1;
	private boolean statut;
	
	public TirePoisBoss(double x, double y, int id) {
		super(x,y,id);
		this.rechargeFeu = new Timer(2000);
		this.rechargeGele = new Timer(3500);
		this.vie = 1200;
		
		this.statut = true;
	}
	
	public int getVie() {
		return vie;
	}



	public void setVie(int vie) {
		this.vie = vie;
	}
	
	public void tirFeu() {
		if (rechargeFeu.hasFinished() && getVie() >0) {
			Entite p = new PoisFeu(this.position.getX(), this.position.getY(), GameWorld.entites.size());
			GameWorld.ajoute(p);
			rechargeFeu.restart();
		}
	}
	
	public void tirGele() {
		if (rechargeGele.hasFinished() && getVie() >0) {
			Entite p = new PoisGele(this.position.getX(), this.position.getY(), GameWorld.entites.size());
			GameWorld.ajoute(p);
			rechargeGele.restart();
		}
	}
	
	@Override
	public void step() {
		tirFeu();
		tirGele();
		this.position.setX(this.position.getX());
		// TODO ici l'objet repart à l'autre bout de l'ecran, a ne pas faire dans votre code
		
		//si la plante n'a plus de vie on change le statut pour la faire disparaitre de la grille
		if (getVie()<=0) {
			this.statut = false; // on change le statut
		}
		
		if (!statut) {
			GameWorld.grille.supprimePlante(this.position.getX(), this.position.getY());
			GameWorld.entites.set(getID(), null);
		}
	}

	@Override
	public void dessine() {
		if (statut) {
			StdDraw.picture(this.position.getX(), this.position.getY()-0.05, "image/tboss.gif", TRUC_SIZE, TRUC_SIZE);
		}
	}

	@Override
	public String toString() {
		return super.toString() + " TirePoisBoss [vie=" + vie + "]";
	}
}
