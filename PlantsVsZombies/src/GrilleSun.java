import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class GrilleSun{

	/*  -on va créer une grille avec une liste pour les lignes
		-chaque élément (ligne) prends une autre liste (colonne)
		-chaque élément (case) à une liste de soleil(zéro ou une infinité)
	*/
	private static List<ArrayList<LinkedList<Soleil>>> grille;
	
	public GrilleSun() {
		//on initialise la grille
		grille = new ArrayList<ArrayList<LinkedList<Soleil>>>(5);
		//les lignes
		for(int i=0;i<5;i++) {
			grille.add(new ArrayList<LinkedList<Soleil>>(9));
		}
		
		//les colonnes et leurs cases
		for(int j=0;j<9;j++) {
			for(int i=0;i<5;i++) {
				LinkedList<Soleil> s = new LinkedList<Soleil>();
				grille.get(i).add(s);
			}
		}
	}
	
	//haut gauche x=0.066 y=0.758
	//haut droite x=0.769 y=0.758
	//bas gauche x=0.061 y=0.288
	//bas droite x=0.767 y=0.289
	//une case x=0.07777 y=0.096
	
	
	public static void clear() {
		//on initialise la grille
				grille = new ArrayList<ArrayList<LinkedList<Soleil>>>(5);
				//les lignes
				for(int i=0;i<5;i++) {
					grille.add(new ArrayList<LinkedList<Soleil>>(9));
				}
				
				//les colonnes et leurs cases
				for(int j=0;j<9;j++) {
					for(int i=0;i<5;i++) {
						LinkedList<Soleil> s = new LinkedList<Soleil>();
						grille.get(i).add(s);
					}
				}
	}
	
	/**
	 * ajouter un soleil e à la grille à la position(x,y)
	 * avec x,y compris dans la grille
	 * 
	 * @param e une Entite soit une plante ou un zombie
	 * @param x un double coordonné x
	 * @param y un double coordonné y
	 * 
	 */
	public void ajoute(Soleil e, double x, double y) {
		int i2 = 0;
		int j2 = 0;
		if(!(x>0.76 || x<0.06 || y>0.76 || y<0.28)) {
			for(double i=0.06;i<0.75;i+=0.07777) {
				for(double j=0.28;j<0.75;j+=0.096) {
					if((i<=x && x<i+0.07777) && (j<=y && y<j+0.096)) {
						grille.get(j2).get(i2).add(e);
					}
					j2++;
				}
				j2=0;
				i2++;
			}
		}
	}
	
	/**
	 * supprime un soleil dans la grille à la position(x,y)
	 * avec x,y compris dans la grille
	 * 
	 * @param x un double coordonné x
	 * @param y un double coordonné y
	 * 
	 */
	
	public void supprime(double x, double y) {
		int i2 = 0;
		int j2 = 0;
		if(!(x>0.76 || x<0.06 || y>0.76 || y<0.28)) {
			for(double i=0.06;i<0.75;i+=0.07777) {
				for(double j=0.28;j<0.75;j+=0.096) {
					if((i<=x && x<i+0.07777) && (j<=y && y<j+0.096)) {
						grille.get(j2).get(i2).remove(0);
					}
					j2++;
				}
				j2=0;
				i2++;
			}
		}
	}
	
	/**
	 * détecte si un soleil est dans la grille à la position(x,y)
	 * avec x,y compris dans la grille
	 * rend vrai si il y a un soleil
	 * 
	 * @param x un double coordonné x
	 * @param y un double coordonné y
	 * 
	 * @return boolean 
	 */
	
	public boolean collision(double x, double y) {
		int i2 = 0;
		int j2 = 0;
		if(!(x>0.76 || x<0.06 || y>0.76 || y<0.28)) {
			for(double i=0.06;i<0.75;i+=0.07777) {
				for(double j=0.28;j<0.75;j+=0.096) {
					if((i<=x && x<i+0.07777) && (j<=y && y<j+0.096)) {
						if(!grille.get(j2).get(i2).isEmpty()) {
							return true;
						}
					}
					j2++;
				}
				j2=0;
				i2++;
			}
		}
		return false;
	}
	
	/**
	 * donne un soleil qui est dans la grille à la position(x,y)
	 * avec x,y compris dans la grille
	 * 
	 * @param x un double coordonné x
	 * @param y un double coordonné y
	 * 
	 * @return Soleil le soleil à la position demandé
	 */
	
	public Soleil get(double x, double y) {
		int i2 = 0;
		int j2 = 0;
		if(!(x>0.76 || x<0.06 || y>0.76 || y<0.28)) {
			for(double i=0.06;i<0.75;i+=0.07777) {
				for(double j=0.28;j<0.75;j+=0.096) {
					if((i<=x && x<i+0.07777) && (j<=y && y<j+0.096)) {
						return grille.get(j2).get(i2).get(0);
					}
					j2++;
				}
				j2=0;
				i2++;
			}
		}
		return null;
	}
	
	@Override
	public String toString() {
		String str="";
		for(int j=0;j<9;j++) {
			for(int i=0;i<5;i++) {
				str += " " +grille.get(i).get(j);
			}
		}
		return str;
	}
	
}
