

public class Tournesol extends Plante{

	private int vie;
	
	private static final double TRUC_SIZE = 0.1;
	private boolean statut;
	private boolean mort;
	private Timer sun;
	private Timer time;
	
	public Tournesol(double x, double y, int id) {
		super(x,y,id);
		this.vie = 300;
		
		this.statut = false;
		this.mort = false;
		sun = new Timer(22000); // timer de 22 secondes
		time = new Timer(2000); //timer de 2 secondes
	}

	public int getVie() {
		return vie;
	}

	public void setVie(int vie) {
		this.vie = vie;
	}
	
	public void pop() {
		Soleil p = new Soleil(this.position.getX(), this.position.getY(), GameWorld.entites.size(), false);
		GameWorld.ajoute(p);
		GameWorld.grilleSun.ajoute(p, p.position.getX(), p.position.getY());
	}
	
	@Override
	public void step() {
		this.position.setX(this.position.getX());
		if(getVie()<=0) {
			mort = true;
		}
		if (sun.hasFinished() && !mort) {
			statut = true;
			sun.restart();
			time.restart();
		}
		if(statut) {
			if(time.hasFinished()) {
				statut =false;
				pop();
			}
		}
		
		if(mort) {
			GameWorld.grille.supprimePlante(this.position.getX(), this.position.getY());
			GameWorld.entites.set(getID(), null);
		}
	}

	@Override
	public void dessine() {
		if(!mort) {
			if (statut) {
				StdDraw.picture(this.position.getX(), this.position.getY()-0.05, "image/tournesolsun.gif", TRUC_SIZE,TRUC_SIZE);
			}else{
				StdDraw.picture(this.position.getX(), this.position.getY()-0.05, "image/tournesol.gif", TRUC_SIZE, TRUC_SIZE);
			}
		}
	}
	@Override
	public String toString() {
		return super.toString() + " Tournesol [vie=" + vie + "]";
	}
	
	
	
}
