import java.awt.event.KeyEvent;

public class Clavier {

	public Clavier() {
		
	}
	
	public char processUserInput(char key) {
		if(StdDraw.isKeyPressed(KeyEvent.VK_BACK_SPACE) || StdDraw.isKeyPressed(KeyEvent.VK_DELETE)) {
			return ' ';
		}
		return key;
	}
}
