
public class Boxe extends Plante{
	private int vie;
	private int degats;
	
	private static final double TRUC_SIZE = 0.2;
	private boolean statut;
	private boolean attaque;
	
	private Timer att;
	
	public Boxe(double x, double y, int id) {
		super(x,y,id);
		this.vie = 1500;
		this.degats = 50;
		
		this.statut = true;
		this.attaque = false;
		
		this.att = new Timer(2000);
	}

	public int getVie() {
		return vie;
	}

	public void setVie(int vie) {
		this.vie = vie;
	}
	
	public int getDegats() {
		return degats;
	}

	public void setDegats(int degats) {
		this.degats = degats;
	}
	
	@Override
	public void step() {
		
		if (getVie()<=0) {
			this.statut = false; // on change le statut
		}
		
		if(GameWorld.grille.collisionZombie(this.position.getX(), this.position.getY())) {
			if(att.hasFinished()) {
				if(GameWorld.grille.tapeZombie(this.position.getX(), this.position.getY()) instanceof ZombieBase){
					((ZombieBase) GameWorld.entites.get(GameWorld.grille.tapeZombie(this.position.getX(), this.position.getY()).getID())).setVie(((ZombieBase) GameWorld.entites.get(GameWorld.grille.tapeZombie(this.position.getX(), this.position.getY()).getID())).getVie() - getDegats());
				}else if(GameWorld.grille.tapeZombie(this.position.getX(), this.position.getY()) instanceof ZombieBlinde) {
					((ZombieBlinde) GameWorld.entites.get(GameWorld.grille.tapeZombie(this.position.getX(), this.position.getY()).getID())).setVie(((ZombieBlinde) GameWorld.entites.get(GameWorld.grille.tapeZombie(this.position.getX(), this.position.getY()).getID())).getVie() - getDegats());
				}else if(GameWorld.grille.tapeZombie(this.position.getX(), this.position.getY()) instanceof ZombieBoss) {
					((ZombieBoss) GameWorld.entites.get(GameWorld.grille.tapeZombie(this.position.getX(), this.position.getY()).getID())).setVie(((ZombieBoss) GameWorld.entites.get(GameWorld.grille.tapeZombie(this.position.getX(), this.position.getY()).getID())).getVie() - getDegats());
				}
				att.restart();
			}
			attaque = true;
		}else if(GameWorld.grille.collisionZombie(this.position.getX()+0.0777, this.position.getY())) {
			if(att.hasFinished()) {
				if(GameWorld.grille.tapeZombie(this.position.getX()+0.0777, this.position.getY()) instanceof ZombieBase){
					((ZombieBase) GameWorld.entites.get(GameWorld.grille.tapeZombie(this.position.getX()+0.0777, this.position.getY()).getID())).setVie(((ZombieBase) GameWorld.entites.get(GameWorld.grille.tapeZombie(this.position.getX()+0.0777, this.position.getY()).getID())).getVie() - getDegats());
				}else if(GameWorld.grille.tapeZombie(this.position.getX()+0.0777, this.position.getY()) instanceof ZombieBlinde) {
					((ZombieBlinde) GameWorld.entites.get(GameWorld.grille.tapeZombie(this.position.getX()+0.0777, this.position.getY()).getID())).setVie(((ZombieBlinde) GameWorld.entites.get(GameWorld.grille.tapeZombie(this.position.getX()+0.0777, this.position.getY()).getID())).getVie() - getDegats());
				}else if(GameWorld.grille.tapeZombie(this.position.getX()+0.0777, this.position.getY()) instanceof ZombieBoss) {
					((ZombieBoss) GameWorld.entites.get(GameWorld.grille.tapeZombie(this.position.getX()+0.0777, this.position.getY()).getID())).setVie(((ZombieBoss) GameWorld.entites.get(GameWorld.grille.tapeZombie(this.position.getX()+0.0777, this.position.getY()).getID())).getVie() - getDegats());
				}
				att.restart();
			}
			attaque = true;
		}else if(GameWorld.grille.collisionZombie(this.position.getX()+0.1444, this.position.getY())) {
			if(att.hasFinished()) {
				if(GameWorld.grille.tapeZombie(this.position.getX()+0.1444, this.position.getY()) instanceof ZombieBase){
					((ZombieBase) GameWorld.entites.get(GameWorld.grille.tapeZombie(this.position.getX()+0.1444, this.position.getY()).getID())).setVie(((ZombieBase) GameWorld.entites.get(GameWorld.grille.tapeZombie(this.position.getX()+0.1444, this.position.getY()).getID())).getVie() - getDegats());
				}else if(GameWorld.grille.tapeZombie(this.position.getX()+0.1444, this.position.getY()) instanceof ZombieBlinde) {
					((ZombieBlinde) GameWorld.entites.get(GameWorld.grille.tapeZombie(this.position.getX()+0.1444, this.position.getY()).getID())).setVie(((ZombieBlinde) GameWorld.entites.get(GameWorld.grille.tapeZombie(this.position.getX()+0.1444, this.position.getY()).getID())).getVie() - getDegats());
				}else if(GameWorld.grille.tapeZombie(this.position.getX()+0.1444, this.position.getY()) instanceof ZombieBoss) {
					((ZombieBoss) GameWorld.entites.get(GameWorld.grille.tapeZombie(this.position.getX()+0.1444, this.position.getY()).getID())).setVie(((ZombieBoss) GameWorld.entites.get(GameWorld.grille.tapeZombie(this.position.getX()+0.1444, this.position.getY()).getID())).getVie() - getDegats());
				}
				att.restart();
			}
			attaque = true;
		}else{
			attaque = false;
			att.restart();
		}
		
		if (!statut) {
			GameWorld.grille.supprimePlante(this.position.getX(), this.position.getY());
			GameWorld.entites.set(getID(), null);
		}
	}

	@Override
	public void dessine() {
		if (statut) {
			if(!attaque) {
				StdDraw.picture(this.position.getX()+0.025, this.position.getY()-0.025, "image/boxe.GIF", TRUC_SIZE, TRUC_SIZE);
			}else {
				StdDraw.picture(this.position.getX()+0.025, this.position.getY()-0.025, "image/boxeAttaque.GIF", TRUC_SIZE, TRUC_SIZE);
			}
		}
	}


	@Override
	public String toString() {
		return super.toString() + " Boxe [vie=" + vie + "]";
	}
}
