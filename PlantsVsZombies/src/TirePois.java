

public class TirePois extends Plante{
	
	private Timer recharge;
	private int vie;
	
	private static final double TRUC_SIZE = 0.1;
	private boolean statut;
	
	public TirePois(double x, double y, int id) {
		super(x,y,id);
		this.recharge = new Timer(1500); // timer de 1.5 secondes
		this.vie = 300;
		
		this.statut = true;
	}

	public Timer getRecharge() {
		return recharge;
	}



	public void setRecharge(Timer recharge) {
		this.recharge = recharge;
	}



	public int getVie() {
		return vie;
	}



	public void setVie(int vie) {
		this.vie = vie;
	}
	
	public void tir() {
		if (recharge.hasFinished() && getVie() >0) {
			Entite p = new Pois(this.position.getX(), this.position.getY(), GameWorld.entites.size());
			GameWorld.ajoute(p);
			recharge.restart();
		}
	}
	
	@Override
	public void step() {
		tir();
		this.position.setX(this.position.getX());
		// TODO ici l'objet repart à l'autre bout de l'ecran, a ne pas faire dans votre code
		
		//si la plante n'a plus de vie on change le statut pour la faire disparaitre de la grille
		if (getVie()<=0) {
			this.statut = false; // on change le statut
		}
		
		if (!statut) {
			GameWorld.grille.supprimePlante(this.position.getX(), this.position.getY());
			GameWorld.entites.set(getID(), null);
		}
	}

	@Override
	public void dessine() {
		if (statut) {
			StdDraw.picture(this.position.getX(), this.position.getY()-0.05, "image/t.gif", TRUC_SIZE, TRUC_SIZE);
		}
	}

	@Override
	public String toString() {
		return super.toString() + " TirePois [vie=" + vie + "]";
	}
	
	
}
