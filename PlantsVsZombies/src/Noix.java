public class Noix extends Plante{
	private int vie;
	
	private static final double TRUC_SIZE = 0.1;
	private boolean statut;
	
	public Noix(double x, double y, int id) {
		super(x,y,id);
		this.vie = 1500;
		
		this.statut = true;
	}

	public int getVie() {
		return vie;
	}

	public void setVie(int vie) {
		this.vie = vie;
	}
	
	@Override
	public void step() {
		this.position.setX(this.position.getX());
		// TODO ici l'objet repart à l'autre bout de l'ecran, a ne pas faire dans votre code
		
		if (getVie()<=0) {
			this.statut = false; // on change le statut
		}
		
		if (!statut) {
			GameWorld.grille.supprimePlante(this.position.getX(), this.position.getY());
			GameWorld.entites.set(getID(), null);
		}
	}

	@Override
	public void dessine() {
		if (statut) {
			StdDraw.picture(this.position.getX(), this.position.getY()-0.05, "image/noix.gif", TRUC_SIZE, TRUC_SIZE);
		}
	}


	@Override
	public String toString() {
		return super.toString() + " Noix [vie=" + vie + "]";
	}
	
	
}
