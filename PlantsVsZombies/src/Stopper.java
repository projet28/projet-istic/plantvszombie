
public class Stopper extends Thread{
	    public Stopper () {
	    }

	    @Override
	    public void run() {
	        // Initialisation
	        	try{     
					if(Main.musique.isPlaying()) Main.musique.stop(); 
				}catch(Exception e){
					e.printStackTrace();
				} 
	    	}
}
