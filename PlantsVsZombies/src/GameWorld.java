import java.util.LinkedList;
import java.util.List;


public class GameWorld {

	// l'ensemble des entites, pour gerer (notamment) l'affichage
	static List<Entite> entites;
	
	// la grille de jeu qui contient les plantes et zombies
	static Grille grille;
	static GrilleSun grilleSun;
	
	// les différents niveaux de jeux
	private Level lvl;
	static int niveau;
	static Timer imageDebut;

	
	//Pour savoir si la partie est gagnee ou pas
	private static boolean gameWon;
	static boolean levelWon;
	// Idem pour savoir si le jeu est perdu (si le jeu n'est ni gagne ni perdu, il est en cours)
	static boolean gameLost;
	
	//vairable boolean pour savoir si la triche est activé
	static boolean triche;
	static boolean vitesse;
	
	//les timers pour planter les plantes
	private Timer tpsP;
	private Timer tpsT;
	private Timer tpsN;
	private Timer tpsB;
	private Timer tpsC;
	private Timer tpsR;
	private Timer tpsV;
	
	//les booleans pour savoir si on peut planter
	private boolean planteT;
	private boolean planteP;
	private boolean planteN;
	private boolean planteB;
	private boolean planteC;
	private boolean planteR;
	private boolean planteV;
	
	private boolean supr;
	private boolean evol;
	private boolean evolTouche;
	private boolean planteG;
	private boolean planteF;
	private boolean planteTB;
	
	private double X,Y,XG,YG;
	
	//int qui permet d'avoir la ressource soleil
	static int totalSun;
	
	static Timer tpstext = new Timer(5000);
	
	private static Timer touche = new Timer(100);

	// constructeur, il faut initialiser notre monde virtuel
	public GameWorld() {
		
		//on initialise le soleil et la triche à false
		triche=false;
		vitesse=false;
		totalSun = 50;
		
		//les timers d'abord à 0
		tpsP = new Timer(0);
		tpsT = new Timer(0);
		tpsN = new Timer(0);
		tpsB = new Timer(0);
		tpsC = new Timer(0);
		tpsR = new Timer(0);
		tpsV = new Timer(0);
		
		planteT = false;
		planteP = false;
		planteN = false;
		planteB = false;
		planteC = false;
		planteR = false;
		planteV = false;
		
		supr = false;
		evol = false;
		evolTouche = false;
		planteG = false;
		planteF = false;
		planteTB = false;
		
		X =0.0;
		Y =0.0;
		XG = 0.0;
		YG = 0.0;

		gameWon=false;
		levelWon=false;
		gameLost=false;
		
		// on cree les collections
		entites = new LinkedList<Entite>();
		
		//on cree les grille
		grille = new Grille();
		grilleSun = new GrilleSun();
		
		//on intialise le niveau
		lvl= new Level();
		imageDebut = new Timer(20000);
	}
	
	/**
	 * range toutes les entites dans le meilleur ordre pour un affichage optimal
	 * avec les soleils en dernier
	 * puis les pois
	 * puis les tondeuses
	 * puis le reste on les ranges dans l'ordre de leur y du plus grand au plus petit
	 * 
	 * @param e une entite
	 */
	public static void ajoute(Entite e) {
		//if(e instanceof Soleil) {
			entites.add(e);
		/*}
		if(e instanceof Tondeuse) {
			int i = 0;
			boolean fin = false;
			while(i < entites.size() && !fin) {
				if(entites.isEmpty()) {
					entites.add(e);
					fin = true;
				}
				if(entites.get(i) instanceof Soleil) {
					entites.add(i,e);
					fin = true;
				}
				i++;
			}
			if(!fin) {
				entites.add(e);
			}
		}
		if(e instanceof Pois) {
			int i = 0;
			boolean fin = false;
			while(i < entites.size() && !fin) {
				if(entites.isEmpty()) {
					entites.add(e);
					fin = true;
				}
				if(entites.get(i) instanceof Tondeuse) {
					entites.add(i,e);
					fin = true;
				}
				i++;
			}
			if(!fin) {
				entites.add(e);
			}
		}
		if(e instanceof Zombie) {
			int i = 0;
			boolean fin = false;
			while(i < entites.size() && !fin) {
				if(entites.isEmpty()) {
					entites.add(e);
					fin = true;
				}
				if(entites.get(i) instanceof Pois && !fin) {
					entites.add(i,e);
					fin = true;
				}
				if(entites.get(i) != null && !fin) {
					if(entites.get(i) instanceof Zombie) {
						if(e.getY()>entites.get(i).getY()) {
							entites.add(i,e);
							fin = true;
						}
					}
				}
				i++;
			}
			if(!fin) {
				entites.add(e);
			}
		}
		if(e instanceof Plante) {
			int i = 0;
			boolean fin = false;
			while(i < entites.size() && !fin) {
				if(entites.isEmpty()) {
					entites.add(e);
					fin = true;
				}
				if(entites.get(i) instanceof Zombie && !fin) {
					entites.add(i,e);
					fin = true;
				}
				if(entites.get(i) != null && !fin) {
					if(entites.get(i) instanceof Plante) {
						if(e.getY()>entites.get(i).getY()) {
							entites.add(i,e);
							fin = true;
						}
					}
				}
				i++;
			}
			if(!fin) {
				entites.add(e);
			}
		}*/
	}

	/**
	 * Gestion des interactions clavier avec l'utilisateur
	 * 
	 * @param key
	 *            Touche pressee par l'utilisateur
	 */
	public void processUserInput(char key) {
		switch (key) {
		case 't':
			if(tpsT.hasFinished() && totalSun >= 50) {
				if (StdDraw.isMousePressed() && !(StdDraw.mouseX()>0.76 || StdDraw.mouseX()<0.06 || StdDraw.mouseY()>0.76 || StdDraw.mouseY()<0.28)) {
					if(!GameWorld.grille.collisionPlante(StdDraw.mouseX(), StdDraw.mouseY()) 
						&& !GameWorld.grille.collisionZombie(StdDraw.mouseX(), StdDraw.mouseY())) {
						Entite t = new Tournesol(StdDraw.mouseX(),StdDraw.mouseY(),entites.size());
						if(triche) {
							((Tournesol) t).setVie(9999);
						}
						ajoute(t);
						grille.ajoute(t, t.position.getX(), t.position.getY());
						totalSun -= 50;
						if(!triche) {
							tpsT = new Timer(5000);
						}
					}	
				}
			}
			break;
		case 'p':
			if(tpsP.hasFinished() && totalSun >= 100) {
				if (StdDraw.isMousePressed() && !(StdDraw.mouseX()>0.76 || StdDraw.mouseX()<0.06 || StdDraw.mouseY()>0.76 || StdDraw.mouseY()<0.28)) {
					if(!GameWorld.grille.collisionPlante(StdDraw.mouseX(), StdDraw.mouseY())
						&& !GameWorld.grille.collisionZombie(StdDraw.mouseX(), StdDraw.mouseY())) {
						Entite t = new TirePois(StdDraw.mouseX(),StdDraw.mouseY(),entites.size());
						if(triche) {
							((TirePois) t).setVie(9999);
						}
						ajoute(t);
						grille.ajoute(t, t.position.getX(), t.position.getY());
						totalSun -= 100;
						if(!triche) {
							tpsP = new Timer(5000);
						}
					}
				}
			}
			break;
		case 'n':
			if(tpsN.hasFinished() && totalSun >= 50) {
				if (StdDraw.isMousePressed() && !(StdDraw.mouseX()>0.76 || StdDraw.mouseX()<0.06 || StdDraw.mouseY()>0.76 || StdDraw.mouseY()<0.28)) {
					if(!GameWorld.grille.collisionPlante(StdDraw.mouseX(), StdDraw.mouseY()) 
						&& !GameWorld.grille.collisionZombie(StdDraw.mouseX(), StdDraw.mouseY())) {
						Entite t = new Noix(StdDraw.mouseX(),StdDraw.mouseY(),entites.size());
						if(triche) {
							((Noix) t).setVie(9999);
						}
						ajoute(t);
						grille.ajoute(t, t.position.getX(), t.position.getY());
						totalSun -= 50;
						if(!triche) {
							tpsN = new Timer(20000);
						}
					}
				}
			}
			break;

		case 'r':
			if(tpsR.hasFinished() && totalSun >= 50 && niveau>=3) {
				if (StdDraw.isMousePressed() && !(StdDraw.mouseX()>0.76 || StdDraw.mouseX()<0.06 || StdDraw.mouseY()>0.76 || StdDraw.mouseY()<0.28)) {
					if(!GameWorld.grille.collisionPlante(StdDraw.mouseX(), StdDraw.mouseY()) 
						&& !GameWorld.grille.collisionZombie(StdDraw.mouseX(), StdDraw.mouseY())) {
						Entite t = new Cerise(StdDraw.mouseX(),StdDraw.mouseY(),entites.size());
						ajoute(t);
						grille.ajoute(t, t.position.getX(), t.position.getY());
						totalSun -= 50;
						if(!triche) {
							tpsR = new Timer(10000);
						}
					}	
				}
			}
			break;
		case 'b':
			if(tpsB.hasFinished() && totalSun >= 150 && niveau>=4) {
				if (StdDraw.isMousePressed() && !(StdDraw.mouseX()>0.76 || StdDraw.mouseX()<0.06 || StdDraw.mouseY()>0.76 || StdDraw.mouseY()<0.28)) {
					if(!GameWorld.grille.collisionPlante(StdDraw.mouseX(), StdDraw.mouseY()) 
						&& !GameWorld.grille.collisionZombie(StdDraw.mouseX(), StdDraw.mouseY())) {
						Entite t = new Boxe(StdDraw.mouseX(),StdDraw.mouseY(),entites.size());
						if(triche) {
							((Boxe) t).setVie(9999);
						}
						ajoute(t);
						grille.ajoute(t, t.position.getX(), t.position.getY());
						totalSun -= 150;
						if(!triche) {
							tpsB = new Timer(10000);
						}
					}	
				}
			}
			break;
			
		case 'c':
			if(tpsC.hasFinished() && totalSun >= 300  && niveau>=5) {
				if (StdDraw.isMousePressed() && !(StdDraw.mouseX()>0.76 || StdDraw.mouseX()<0.06 || StdDraw.mouseY()>0.76 || StdDraw.mouseY()<0.28)) {
					if(!GameWorld.grille.collisionPlante(StdDraw.mouseX(), StdDraw.mouseY()) 
						&& !GameWorld.grille.collisionZombie(StdDraw.mouseX(), StdDraw.mouseY())) {
						Entite t = new Carni(StdDraw.mouseX(),StdDraw.mouseY(),entites.size());
						if(triche) {
							((Carni) t).setVie(9999);
						}
						ajoute(t);
						grille.ajoute(t, t.position.getX(), t.position.getY());
						totalSun -= 300;
						if(!triche) {
							tpsC = new Timer(20000);
						}
					}	
				}
			}
			break;	
			
		case 'v':
			if(tpsV.hasFinished() && totalSun >= 300  && niveau>=6) {
				if (StdDraw.isMousePressed() && !(StdDraw.mouseX()>0.76 || StdDraw.mouseX()<0.06 || StdDraw.mouseY()>0.76 || StdDraw.mouseY()<0.28)) {
					if(!GameWorld.grille.collisionPlante(StdDraw.mouseX(), StdDraw.mouseY()) 
						&& !GameWorld.grille.collisionZombie(StdDraw.mouseX(), StdDraw.mouseY())) {
						Entite t = new Champi(StdDraw.mouseX(),StdDraw.mouseY(),entites.size());
						if(triche) {
							((Champi) t).setVie(9999);
						}
						ajoute(t);
						grille.ajoute(t, t.position.getX(), t.position.getY());
						totalSun -= 300;
						if(!triche) {
							tpsV = new Timer(20000);
						}
					}	
				}
			}
			break;	
		case 'e':
			if(niveau>=9) evolTouche = !evolTouche;
			break;
		case 'a':
				if(!triche) {
					totalSun = 9999;
					tpsP = new Timer(0);
					tpsN = new Timer(0);
					tpsT = new Timer(0);
					tpsC = new Timer(0);
					tpsB = new Timer(0);
					tpsV = new Timer(0);
					for (Entite entite : entites)
						if( entite instanceof TirePois) {
							((TirePois) entite).setVie(9999);
						}else if(entite instanceof Tournesol) {
							((Tournesol) entite).setVie(9999);
						}else if(entite instanceof Noix) {
							((Noix) entite).setVie(9999);
						}else if(entite instanceof Boxe) {
							((Boxe) entite).setVie(9999);
						}else if(entite instanceof Carni) {
							((Carni) entite).setVie(9999);
						}else if(entite instanceof Champi) {
							((Champi) entite).setVie(9999);
						}
					triche = !triche;
				}else {
					totalSun = 50;
					for (Entite entite : entites)
						if( entite instanceof TirePois) {
							((TirePois) entite).setVie(300);
						}else if( entite instanceof Tournesol) {
							((Tournesol) entite).setVie(300);
						}else if( entite instanceof Noix) {
							((Noix) entite).setVie(1500);
						}else if(entite instanceof Boxe) {
							((Boxe) entite).setVie(1500);
						}else if(entite instanceof Carni) {
							((Carni) entite).setVie(500);
						}else if(entite instanceof Champi) {
							((Champi) entite).setVie(300);
						}
					triche = !triche;
				}
			break;
		case 'z':
			if(!Level.debut.hasFinished()) {
			Level.debut = new Timer(0);
			imageDebut = new Timer(0);
			}else {
				if(!vitesse) {
					for (Entite entite : entites)
						if( entite instanceof ZombieBase) {
							((ZombieBase) entite).setDegat(1000);
							((ZombieBase) entite).setTRUC_MOVE_X(0.00538);
						}else if( entite instanceof ZombieBlinde) {
							((ZombieBlinde) entite).setDegat(1000);
							((ZombieBlinde) entite).setTRUC_MOVE_X(0.00538);
						}else if( entite instanceof ZombieBoss) {
							((ZombieBoss) entite).setDegat(1500);
							((ZombieBoss) entite).setTRUC_MOVE_X(0.01074);
						}else if( entite instanceof ZombieBossPaul) {
							((ZombieBossPaul) entite).setDegat(1500);
							((ZombieBossPaul) entite).setTRUC_MOVE_X(0.01074);
						}
					vitesse = !vitesse;
				}else {
					for (Entite entite : entites)
						if( entite instanceof ZombieBase) {
							((ZombieBase) entite).setDegat(30);
							((ZombieBase) entite).setTRUC_MOVE_X(0.00038);
						}else if( entite instanceof ZombieBlinde) {
							((ZombieBlinde) entite).setDegat(30);
							((ZombieBlinde) entite).setTRUC_MOVE_X(0.00038);
						}else if( entite instanceof ZombieBoss) {
							((ZombieBoss) entite).setDegat(100);
							((ZombieBoss) entite).setTRUC_MOVE_X(0.00074);
						}else if( entite instanceof ZombieBossPaul) {
							((ZombieBossPaul) entite).setDegat(100);
							((ZombieBossPaul) entite).setTRUC_MOVE_X(0.00074);
						}
					vitesse = !vitesse;
				}
			}
			break;
		default:
			System.out.println("Touche non prise en charge");
			break;
		}
	}
	
	/**
	 * Gestion des interactions souris avec l'utilisateur (la souris a été cliquée)
	 * 
	 * @param x position en x de la souris au moment du clic
	 * @param y position en y de la souris au moment du clic
	 */
	public void processMouseClick(double x, double y) {
		//enlevé la musique
				if (x > 0.62 && x < 0.78 && y < 0.96 && y > 0.91 && touche.hasFinished()) {
					try {
						if(Main.musique.isPlaying()) {
							Main.musique.stop();
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}	
					touche.restart();
				}
		
		//supprimer une entité
		if (x > 0.8 && x < 0.99 && y < 0.30 && y > 0.22 && touche.hasFinished()) {
			supr = !supr;	
			touche.restart();
		}
		if(supr) {
			if(grille.getPlante(x, y) != null) {
				entites.set(grille.getPlante(x, y).getID(), null);
				grille.supprimePlante(x, y);
				totalSun+=50;
				touche.restart();
			}
		}
		// ajout du Tournesol en utilisant la souris

		if (tpsT.hasFinished() && totalSun >= 50 && touche.hasFinished()) {
			if (x > 0.01 && x < 0.11 && y < 0.30 && y > 0.22) {
				planteT = true;	
				touche.restart();
			}
		}
		if(planteT && touche.hasFinished()) {
			if(!(x>0.76 || x<0.06 || y>0.76 || y<0.28)) {
				if (!GameWorld.grille.collisionPlante(x, y)
						&& !GameWorld.grille.collisionZombie(x, y)) {
					Entite T = new Tournesol(x, y, entites.size());
					if (triche) {
						((Tournesol) T).setVie(9999);
					}
					ajoute(T);
					grille.ajoute(T, x, y);
					totalSun -= 50;
					if (!triche) {
						tpsT = new Timer(5000);
					}
					planteT = false;
					touche.restart();
				}
			}
		}
		
		//le tir-pois
		if (tpsP.hasFinished() && totalSun >= 100 && touche.hasFinished()) {
			if (x >= 0.12 && x < 0.23 && y < 0.30 && y > 0.22) {
				planteP = true;	
				touche.restart();
			}
		}
		if(planteP && touche.hasFinished()) {
			if(!(x>0.76 || x<0.06 || y>0.76 || y<0.28)) {
				if (!GameWorld.grille.collisionPlante(x, y)
						&& !GameWorld.grille.collisionZombie(x, y)) {
					Entite T = new TirePois(x, y, entites.size());
					if (triche) {
						((TirePois) T).setVie(9999);
					}
					ajoute(T);
					grille.ajoute(T, x, y);
					totalSun -= 100;
					if (!triche) {
						tpsP = new Timer(5000);
					}
					planteP = false;
					touche.restart();
				}
			}
		}
		
		//la noix
		if (tpsN.hasFinished() && totalSun >= 50 && touche.hasFinished()) {
			if (x >= 0.24 && x < 0.34 && y < 0.30 && y > 0.22) {
				planteN = true;	
				touche.restart();
			}
		}
		if(planteN && touche.hasFinished()) {
			if(!(x>0.76 || x<0.06 || y>0.76 || y<0.28)) {
				if (!GameWorld.grille.collisionPlante(x, y)
						&& !GameWorld.grille.collisionZombie(x, y)) {
					Entite T = new Noix(x, y, entites.size());
					if (triche) {
						((Noix) T).setVie(9999);
					}
					ajoute(T);
					grille.ajoute(T, x, y);
					totalSun -= 50;
					if (!triche) {
						tpsN = new Timer(20000);
					}
					planteN = false;
					touche.restart();
				}
			}
		}
		
		//la plante boxe
		if (tpsB.hasFinished() && totalSun >= 150  && niveau>=4 && touche.hasFinished()) {
			if (x >= 0.46 && x < 0.56 && y < 0.30 && y > 0.22) {
				planteB = true;	
				touche.restart();
			}
		}
		if(planteB && touche.hasFinished()) {
			if(!(x>0.76 || x<0.06 || y>0.76 || y<0.28)) {
				if (!GameWorld.grille.collisionPlante(x, y)
						&& !GameWorld.grille.collisionZombie(x, y)) {
					Entite T = new Boxe(x, y, entites.size());
					if (triche) {
						((Boxe) T).setVie(9999);
					}
					ajoute(T);
					grille.ajoute(T, x, y);
					totalSun -= 150;
					if (!triche) {
						tpsB = new Timer(10000);
					}
					planteB = false;
					touche.restart();
				}
			}
		}
		
		//la plante carni
		if (tpsC.hasFinished() && totalSun >= 300  && niveau>=5 && touche.hasFinished()) {
			if (x >= 0.57 && x < 0.67 && y < 0.30 && y > 0.22) {
				planteC = true;	
				touche.restart();
			}
		}
		if(planteC && touche.hasFinished()) {
			if(!(x>0.76 || x<0.06 || y>0.76 || y<0.28)) {
				if (!GameWorld.grille.collisionPlante(x, y)
						&& !GameWorld.grille.collisionZombie(x, y)) {
					Entite T = new Carni(x, y, entites.size());
					if (triche) {
						((Carni) T).setVie(9999);
					}
					ajoute(T);
					grille.ajoute(T, x, y);
					totalSun -= 300;
					if (!triche) {
						tpsC = new Timer(20000);
					}
					planteC = false;
					touche.restart();
				}
			}
		}
		
		//la cerise
		if (tpsR.hasFinished() && totalSun >= 50 && niveau>=3 && touche.hasFinished()) {
			if (x >= 0.35 && x < 0.45 && y < 0.30 && y > 0.22) {
				planteR = true;	
				touche.restart();
			}
		}
		if(planteR && touche.hasFinished()) {
			if(!(x>0.76 || x<0.06 || y>0.76 || y<0.28)) {
				if (!GameWorld.grille.collisionPlante(x, y)
						&& !GameWorld.grille.collisionZombie(x, y)) {
					Entite T = new Cerise(x, y, entites.size());
					ajoute(T);
					grille.ajoute(T, x, y);
					totalSun -= 50;
					if (!triche) {
						tpsR = new Timer(10000);
					}
					planteR = false;
					touche.restart();
				}
			}
		}
		
		//le champi
				if (tpsV.hasFinished() && totalSun >= 300 && niveau>=6 && touche.hasFinished()) {
					if (x >= 0.68 && x < 0.78 && y < 0.30 && y > 0.22) {
						planteV = true;	
						touche.restart();
					}
				}
				if(planteV && touche.hasFinished()) {
					if(!(x>0.76 || x<0.06 || y>0.76 || y<0.28)) {
						if (!GameWorld.grille.collisionPlante(x, y)
								&& !GameWorld.grille.collisionZombie(x, y)) {
							Entite T = new Champi(x, y, entites.size());
							if (triche) {
								((Champi) T).setVie(9999);
							}
							ajoute(T);
							grille.ajoute(T, x, y);
							totalSun -= 50;
							if (!triche) {
								tpsV = new Timer(20000);
							}
							planteV = false;
							touche.restart();
						}
					}
				}
				
		//LES EVOLUTIONS
		if(touche.hasFinished()) {
			if(GameWorld.grille.getPlante(x, y) != null) {
				if(planteG && tpsP.hasFinished() && x<GameWorld.grille.getPlante(x, y).getX()) {
					if (GameWorld.grille.getPlante(x, y) instanceof TirePois && totalSun >= 300){
						entites.set(GameWorld.grille.getPlante(x, y).getID(), null);
						grille.supprimePlante(x, y);
						Entite T = new TirePoisGele(x, y, entites.size());
						if (triche) {
							((TirePoisGele) T).setVie(9999);
						}
						ajoute(T);
						grille.ajoute(T, x, y);
						if (!triche) {
							tpsP = new Timer(5000);
						}
						totalSun -= 300;
						touche.restart();
					}
				}
				if(planteF && tpsP.hasFinished() && x>GameWorld.grille.getPlante(x, y).getX()) {
					if (GameWorld.grille.getPlante(x, y) instanceof TirePois && totalSun >= 300){
						entites.set(GameWorld.grille.getPlante(x, y).getID(), null);
						grille.supprimePlante(x, y);
						Entite T = new TirePoisFeu(x, y, entites.size());
						if (triche) {
							((TirePoisFeu) T).setVie(9999);
						}
						ajoute(T);
						grille.ajoute(T, x, y);
						if (!triche) {
							tpsP = new Timer(5000);
						}
						totalSun -= 300;
						touche.restart();
					}
				}
				
				if(planteTB && tpsP.hasFinished() && y<GameWorld.grille.getPlante(x, y).getY()) {
					if ((GameWorld.grille.getPlante(x, y) instanceof TirePoisGele || GameWorld.grille.getPlante(x, y) instanceof TirePoisFeu) && totalSun >= 600){
						entites.set(GameWorld.grille.getPlante(x, y).getID(), null);
						grille.supprimePlante(x, y);
						Entite T = new TirePoisBoss(x, y, entites.size());
						if (triche) {
							((TirePoisBoss) T).setVie(9999);
						}
						ajoute(T);
						grille.ajoute(T, x, y);
						if (!triche) {
							tpsP = new Timer(10000);
						}
						totalSun -= 600;
						touche.restart();
					}
				}
			}
			if(evol && evolTouche) {
				if (GameWorld.grille.getPlante(x, y) instanceof Tournesol && totalSun>=100) {
					((Tournesol) GameWorld.grille.getPlante(x, y)).setVie(600);
					totalSun-=100;
					if (!triche) {
						tpsT = new Timer(10000);
					}
					touche.restart();
				}
				if (GameWorld.grille.getPlante(x, y) instanceof Boxe && totalSun>=300) {
					((Boxe) GameWorld.grille.getPlante(x, y)).setVie(3000);
					totalSun-=100;
					if (!triche) {
						tpsB = new Timer(10000);
					}
					touche.restart();
				}
				if (GameWorld.grille.getPlante(x, y) instanceof Carni && totalSun>=600) {
					((Carni) GameWorld.grille.getPlante(x, y)).setVie(1000);
					totalSun-=100;
					if (!triche) {
						tpsC = new Timer(10000);
					}
					touche.restart();
				}
				if (GameWorld.grille.getPlante(x, y) instanceof Noix && totalSun>=100) {
					((Noix) GameWorld.grille.getPlante(x, y)).setVie(3000);
					totalSun-=100;
					if (!triche) {
						tpsN = new Timer(10000);
					}
					touche.restart();
				}
			}
		}
	}
	
	/**
	 * Gestion des interactions souris avec l'utilisateur
	 * 
	 * @param x position en x de la souris au moment du clic
	 * @param y position en y de la souris au moment du clic
	 */
	public void processMouse(double x, double y) {
		if (GameWorld.grille.getPlante(x, y)== null) {
			evol = false;
			planteG = false;
			planteF = false;
			planteTB = false;
			X = 0;
			Y = 0;
			XG = 0;
			YG = 0;
		}	
		if (GameWorld.grille.getPlante(x, y) instanceof Plante) {
			StdDraw.setPenColor(StdDraw.BLACK);
			StdDraw.text(x, y, GameWorld.grille.getPlante(x, y).toString());
			if(niveau>=7) {
				if (GameWorld.grille.getPlante(x, y) instanceof TirePois) {
					planteG = true;
					XG = GameWorld.grille.getPlante(x, y).getX()-0.07;
					YG = GameWorld.grille.getPlante(x, y).getY()-0.05;
				}
			}
			if(niveau>=8) {
				if (GameWorld.grille.getPlante(x, y) instanceof TirePois) {
					planteF = true;
					X = GameWorld.grille.getPlante(x, y).getX()+0.07;
					Y = GameWorld.grille.getPlante(x, y).getY()-0.05;
				}
			}
			if(niveau>=9) {
				if (!(GameWorld.grille.getPlante(x, y) instanceof TirePois)) {
					evol = true;
				}
			}
			if(niveau>=10) {
				if ((GameWorld.grille.getPlante(x, y) instanceof TirePoisGele || GameWorld.grille.getPlante(x, y) instanceof TirePoisFeu) ) {
					planteTB = true;
					X = GameWorld.grille.getPlante(x, y).getX();
					Y = GameWorld.grille.getPlante(x, y).getY()-0.1;
				}
			}
		}
		if (GameWorld.grille.getZombie(x, y, 0) instanceof Zombie) {
			StdDraw.setPenColor(StdDraw.BLACK);
			StdDraw.text(x, y+0.025, GameWorld.grille.getZombie(x, y, 0).toString());
		}
	}

	// on fait bouger/agir toutes les entites
	public void step() {

		//on appelle le niveau désiré et on teste si on gagne ou l'on perd
		if(lvl.perdu()) {
			gameLost = true;
		}
		if(lvl.gagne()) {
			levelWon = true;
			if(niveau ==10) {
				niveau++;
			}
		}
		if(niveau == 11) {
			gameWon = true;
		}

		lvl.launch(niveau);
		
		
		for (int i = 0; i<entites.size();i++) {
			if(entites.get(i) != null) entites.get(i).step();
		}
	}

	// dessine les entites du jeu
	public void dessine() {

		// Ici vous pouvez afficher du décors
		
		StdDraw.picture(0.5, 0.5, "image/map.jpg");
		
		//les messages
		
				StdDraw.picture(0.7, 0.87, "image/menu/BoutonMusiqueOff.png");
				if (triche) {
					StdDraw.picture(0.9, 0.85, "image/niveaux/Triche.png");
				}
				if (vitesse) {
					StdDraw.picture(0.9, 0.79, "image/niveaux/ZombiesTriche.png");
				}
				if(supr) {
					StdDraw.picture(0.9, 0.73, "image/niveaux/Arrache.png");
				}
				if(evolTouche) {
					StdDraw.picture(0.9, 0.67, "image/niveaux/evo.png");
				}
				if (niveau == 1) {
					StdDraw.picture(0.45, 0.87, "image/niveaux/BoutonNiveau 1.png");
				}
				if (niveau == 2) {
					StdDraw.picture(0.45, 0.87, "image/niveaux/BoutonNiveau 2.png");
					if (!tpstext.hasFinished()) {
						StdDraw.picture(0.5, 0.5, "image/niveaux/LockTondeuse.png");
					}
				}
				if (niveau == 3) {
					StdDraw.picture(0.45, 0.87, "image/niveaux/BoutonNiveau 3.png");
					if (!tpstext.hasFinished()) {
						StdDraw.picture(0.5, 0.5, "image/niveaux/LockCerise.png");

					}
				}
				if (niveau == 4) {
					StdDraw.picture(0.45, 0.87, "image/niveaux/BoutonNiveau 4.png");
					if (!tpstext.hasFinished()) {
						StdDraw.picture(0.5, 0.5, "image/niveaux/LockBoxe.png");
					}
				}
				if (niveau == 5) {
					StdDraw.picture(0.45, 0.87, "image/niveaux/BoutonNiveau 5.png");
					if (!tpstext.hasFinished()) {
						StdDraw.picture(0.5, 0.5, "image/niveaux/LockCarnis.png");
					}
				}
				if (niveau == 6) {
					StdDraw.picture(0.45, 0.87, "image/niveaux/BoutonNiveau 6.png");
					if (!tpstext.hasFinished()) {
						StdDraw.picture(0.5, 0.5, "image/niveaux/LockChampis.png");
					}
				}
				if (niveau == 7) {
					StdDraw.picture(0.45, 0.87, "image/niveaux/BoutonNiveau 7.png");
					if (!tpstext.hasFinished()) {
						StdDraw.picture(0.5, 0.5, "image/niveaux/LockIce.png");
					}
				}
				if (niveau == 8) {
					StdDraw.picture(0.45, 0.87, "image/niveaux/BoutonNiveau 8.png");
					if (!tpstext.hasFinished()) {
						StdDraw.picture(0.5, 0.5, "image/niveaux/LockFeu.png");
					}
				}
				if (niveau == 9) {
					StdDraw.picture(0.45, 0.87, "image/niveaux/BoutonNiveau 9.png");
					if (!tpstext.hasFinished()) {
						StdDraw.picture(0.5, 0.5, "image/niveaux/LockEvolution.png");
					}
				}
				if (niveau == 99) {
					StdDraw.picture(0.45, 0.87, "image/niveaux/BoutonNiveauSurvie.png");
				}
				if (niveau == 10) {
					StdDraw.picture(0.45, 0.87, "image/niveaux/BoutonNiveau 10.png");
					if (!tpstext.hasFinished()) {
						StdDraw.picture(0.5, 0.5, "image/niveaux/LockBoss.png");
					} else {
						if (!tpstext.hasFinished()) {
							StdDraw.picture(0.5, 0.5, "image/niveaux/PoisPaulux.png");
						}
					}
				}

		
		StdDraw.picture(0.9, 0.2, "image/croix.png");
		
		//affiche la reserve de Soleil
		 
		StdDraw.picture(0.1, 0.85, "image/reserve.png");
		StdDraw.setPenColor(StdDraw.WHITE);
		StdDraw.text(0.12, 0.85, "" + totalSun);
		
				// le Tournesol
					StdDraw.picture(0.11, 0.105, "image/imageTournesol.png");
					if(!tpsT.hasFinished()) {
						StdDraw.picture(0.055, 0.175, "image/RTo.gif");
					}
				// Le TirePois
					StdDraw.picture(0.17, 0.175, "image/imageT.png");
					if(!tpsP.hasFinished()) {
						StdDraw.picture(0.17, 0.175, "image/RT.gif");
					}
				// La Noix
					StdDraw.picture(0.29, 0.175, "image/imageNoix.png");
					if(!tpsN.hasFinished()) {
						StdDraw.picture(0.29, 0.175, "image/RN.gif",0.12,0.075);
					}
				// La Cerise
					if(niveau>=3) {
						StdDraw.picture(0.405, 0.175, "image/imageC.png");
						if(!tpsR.hasFinished()) {
							StdDraw.picture(0.405, 0.175, "image/RC.gif",0.11,0.07);
							
						}
					}
					// La Boxe
					if(niveau>=4) {
						StdDraw.picture(0.52, 0.175, "image/imageB.png");
						if(!tpsB.hasFinished()) {
							StdDraw.picture(0.52, 0.175, "image/Rb.gif");
						}
					}
					// La Carni
					if(niveau>=5) {
						StdDraw.picture(0.638, 0.175, "image/imageCarni.png");
			
						if(!tpsC.hasFinished()) {
							StdDraw.picture(0.638, 0.175, "image/RCarni.gif");
					}
					}
					// La Champi
					if(niveau>=6) {
						StdDraw.picture(0.76, 0.175, "image/ImageChampi.png");

					}

		// les evolutions
			if(planteG) {
				StdDraw.picture(XG, YG, "image/imageG.png");
			}
			
			if(planteF) {
				StdDraw.picture(X, Y, "image/imageF.png");
			}
			
			if(planteTB) {
				StdDraw.picture(X, Y, "image/imageBoss.png");
			}
			
		//on affiche les premiers zombies jusqu'à ce qu'ils bougent
		if(!imageDebut.hasFinished()) {
			StdDraw.picture(0.8, 0.5, "image/zstart.gif", 0.10, 0.10);
		}
		
		

		
		// affiche les entites
		List<Entite> l = entites;
		
		for (int i = 0; i<l.size();i++) {
			if(l.get(i) != null) l.get(i).dessine();
		}
		l = entites;
	}
	
	public static boolean levelWon() {
		return levelWon;
	}

	public static boolean gameWon() {
		return gameWon;
	}
	
	public static boolean gameLost() {
		return gameLost;
	}

}
