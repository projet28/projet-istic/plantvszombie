
public class TirePoisFeu extends Plante{
	private Timer recharge;
	private int vie;
	
	private static final double TRUC_SIZE = 0.1;
	private boolean statut;
	
	public TirePoisFeu(double x, double y, int id) {
		super(x,y,id);
		this.recharge = new Timer(2000);
		this.vie = 600;
		
		this.statut = true;
	}

	public Timer getRecharge() {
		return recharge;
	}



	public void setRecharge(Timer recharge) {
		this.recharge = recharge;
	}



	public int getVie() {
		return vie;
	}



	public void setVie(int vie) {
		this.vie = vie;
	}
	
	public void tirFeu() {
		if (recharge.hasFinished() && getVie() >0) {
			Entite p = new PoisFeu(this.position.getX(), this.position.getY(), GameWorld.entites.size());
			GameWorld.ajoute(p);
			recharge.restart();
		}
	}
	
	@Override
	public void step() {
		tirFeu();
		this.position.setX(this.position.getX());
		// TODO ici l'objet repart à l'autre bout de l'ecran, a ne pas faire dans votre code
		
		//si la plante n'a plus de vie on change le statut pour la faire disparaitre de la grille
		if (getVie()<=0) {
			this.statut = false; // on change le statut
		}
		
		if (!statut) {
			GameWorld.grille.supprimePlante(this.position.getX(), this.position.getY());
			GameWorld.entites.set(getID(), null);
		}
	}

	@Override
	public void dessine() {
		if (statut) {
			StdDraw.picture(this.position.getX(), this.position.getY()-0.05, "image/tfeu.gif", TRUC_SIZE, TRUC_SIZE);
		}
	}

	@Override
	public String toString() {
		return super.toString() + " TirePois de feu [vie=" + vie + "]";
	}
}
