import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Compte {
	
	private File fichier;
	private BufferedWriter write;
	private BufferedReader read;
	
	
	Compte(String nom, boolean nouv){
		this.fichier = new File(nom+".txt");
		
		if(nouv) {
			try {
				this.write = new BufferedWriter(new FileWriter(fichier));
				write.write(nom);
				write.newLine();
				write.write("00000000000");
				write.newLine();
				write.write("0");
				write.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
	}
	
	public void niveauSup(boolean nouv) {
		if(nouv) {
			String nom = new String();
			String classement = new String();
			String str = new String();
			String str2 = new String();
			
			int i=0;
			try {
				this.read = new BufferedReader(new FileReader(fichier));
				nom = read.readLine();
				str = read.readLine();
				while(str.charAt(i)!='0') {
					str2 += str.charAt(i);
					i++;
				}
				str2 += i+1;
				i++;
				while(i<=11) {
					str2 += "0";
					i++;
				}
				classement = read.readLine();
				read.close();
				
				this.write = new BufferedWriter(new FileWriter(fichier));
				write.write(nom);
				write.newLine();
				write.write(str2);
				write.newLine();
				write.write(classement);
				write.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.exit(0);
			}
		}
	}
	
	public int getNiveau() {
		String str = new String();
		int i=0;
		try {
			this.read = new BufferedReader(new FileReader(fichier));
			read.readLine();
			str = read.readLine();
			read.close();
			while(str.charAt(i)!='0') {
				i++;
			}
			return i+1;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(0);
		}
		return 0;
	}
	
	public void setScore(String newScore) {
		String nom = new String();
		String niveau = new String();
		String score = new String();
		try {
			this.read = new BufferedReader(new FileReader(fichier));
			nom = read.readLine();
			niveau = read.readLine();
			score = read.readLine();
			read.close();
			
			if(Integer.parseInt(score)<Integer.parseInt(newScore)) {
				this.write = new BufferedWriter(new FileWriter(fichier));
				write.write(nom);
				write.newLine();
				write.write(niveau);
				write.newLine();
				write.write(newScore);
				write.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(0);
		}
	}
	
	public String getScore() {
		String str = new String();
		try {
			this.read = new BufferedReader(new FileReader(fichier));
			read.readLine();
			read.readLine();
			str = read.readLine();
			read.close();
			return str;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(0);
		}
		return "0";
	}
	
	public void setClassement(String[] newClassement) {
		List<String> ancien = new ArrayList<String>();
		String tmp = new String();
		String tmp2 = new String();
		String nouvNom = newClassement[0];
		String nouvScore = newClassement[1];
		try {
			int i=1;
			int j= 1;
			boolean bool = false;
			boolean vide = false;
			this.read = new BufferedReader(new FileReader(fichier));
			while(i<20) {
				tmp = read.readLine();
				tmp2 = read.readLine();
				if(tmp2 != null && nouvScore != null) {
					if(Integer.parseInt(tmp2) < Integer.parseInt(nouvScore) && !bool) {
						j=i;
						bool = true;
					}
				}
				if(tmp2 == null && !bool && !vide) {
					j=i;
					vide = true;
				}
				if(tmp != null && tmp2 != null) {
					ancien.add(tmp);
					ancien.add(tmp2);
				}
				
				i+=2;
			}
			read.close();
			if(bool) {
				ancien.add(j-1,nouvNom);
				ancien.add(j,nouvScore);
			}else if(vide){
				ancien.add(nouvNom);
				ancien.add(nouvScore);
			}
			
			this.write = new BufferedWriter(new FileWriter(fichier));
			i=1;
			while(i<ancien.size()) {
				write.write(ancien.get(i-1));
				write.newLine();
				write.write(ancien.get(i));
				write.newLine();
				i+=2;
			}
			write.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(0);
		}
	}
	
	public String[] getClassement() {
		String[] classement = new String[20];
		String str1 = new String();
		String str2 = new String();
		int i = 0;
		try {
			this.read = new BufferedReader(new FileReader(fichier));
			while(i<20) {
				if((str1 = read.readLine()) == null) {
					str1 ="";
				}
				if((str2 = read.readLine()) == null) {
					str2 ="";
				}
				classement[i]=str1;
				classement[i+1]=str2;
				i+=2;
			}
			read.close();
			return classement;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(0);
		}
		return null;
	}
	
}
