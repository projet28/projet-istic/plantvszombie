

public class Soleil extends Entite{

	private int ID;
	private double y;
	
	private static final double TRUC_MOVE_Y = 0.00152;
	private static final double TRUC_SIZE = 0.05;
	private boolean statut;
	private Timer timer;
	
	private boolean descend;
	
	public Soleil(double x, double y, int id, boolean descend) {
		super(x,y);
		this.ID = id;
		this.y = 1;
		
		this.statut = true;
		timer = new Timer(20000); // timer de 20 secondes
		
		this.descend = descend;
	}
	
	public int getID() {
		return ID;
	}



	public void setID(int iD) {
		ID = iD;
	}
	
	@Override
	public void step() {
		// TODO ici l'objet repart à l'autre bout de l'ecran, a ne pas faire dans votre code
		if (StdDraw.isMousePressed() && GameWorld.grilleSun.collision(StdDraw.mouseX(), StdDraw.mouseY())
				&& ((Soleil) GameWorld.grilleSun.get(StdDraw.mouseX(), StdDraw.mouseY())).getID() == getID()) {
			GameWorld.totalSun += 25;
			statut = false;
		} else {
			if(timer.hasFinished()) {
				GameWorld.totalSun += 25;
				statut = false;
			}
		}
		if(!statut && ((Soleil) GameWorld.grilleSun.get(this.position.getX(), this.position.getY())).getID() == getID()) {
			GameWorld.grilleSun.supprime(this.position.getX(), this.position.getY());
			GameWorld.entites.set(getID(), null);
		}
	}

	@Override
	public void dessine() {
		if (statut) {
			if(descend) {
				if(y>=this.position.getY()) {
					y = y - TRUC_MOVE_Y;
				}
				StdDraw.picture(this.position.getX(), y-0.05, "image/sun.gif", TRUC_SIZE, TRUC_SIZE);
			}else {
				StdDraw.picture(this.position.getX(), this.position.getY()-0.1, "image/sun.gif", TRUC_SIZE, TRUC_SIZE);
			}
		}
	}

	@Override
	public String toString() {
		return "Soleil [id=" + getID() + "]";
	}
	
	
	
}
