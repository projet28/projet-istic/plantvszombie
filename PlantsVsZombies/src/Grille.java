import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javafx.util.Pair;


public class Grille{

	/*  -on va créer une grille avec une liste pour les lignes
		-chaque élément (ligne) prends une autre liste (colonne)
		-chaque élément (case) à une pair qui contient une plante(zéro ou une pas plus)
		 et une liste de zombie(zéro ou une infinité)
	*/
	private static List<ArrayList<Pair<Plante, LinkedList<Zombie>>>> grille;
	
	public Grille() {
		//on initialise la grille
		grille = new ArrayList<ArrayList<Pair<Plante, LinkedList<Zombie>>>>(5);
		//les lignes
		for(int i=0;i<5;i++) {
			grille.add(new ArrayList<Pair<Plante, LinkedList<Zombie>>>(9));
		}
		
		//les colonnes et leurs cases
		for(int j=0;j<9;j++) {
			for(int i=0;i<5;i++) {
				LinkedList<Zombie> z = new LinkedList<Zombie>();
				grille.get(i).add(new Pair<>(null, z));
			}
		}
	}
	
	//haut gauche x=0.066 y=0.758
	//haut droite x=0.769 y=0.758
	//bas gauche x=0.061 y=0.288
	//bas droite x=0.767 y=0.289
	//une case x=0.07777 y=0.096
	
	public static void clear() {
		//on initialise la grille
				grille = new ArrayList<ArrayList<Pair<Plante, LinkedList<Zombie>>>>(5);
				//les lignes
				for(int i=0;i<5;i++) {
					grille.add(new ArrayList<Pair<Plante, LinkedList<Zombie>>>(9));
				}
				
				//les colonnes et leurs cases
				for(int j=0;j<9;j++) {
					for(int i=0;i<5;i++) {
						LinkedList<Zombie> z = new LinkedList<Zombie>();
						grille.get(i).add(new Pair<>(null, z));
					}
				}
	}
	/**
	 * ajouter une entité e à la grille à la position(x,y)
	 * avec x,y compris dans la grille
	 * si c'est une plante on peut en mettre que si il y en a pas déjà une 
	 * si c'est un zombie on peut en mettre plusieurs 
	 * 
	 * @param e une Entite soit une plante ou un zombie
	 * @param x un double coordonné x
	 * @param y un double coordonné y
	 * 
	 */
	public void ajoute(Entite e, double x, double y) {
		if( e instanceof Plante) {
			if(!collisionPlante(x,y)) {
				int i2 = 0;
				int j2 = 0;
				if(!(x>0.76 || x<0.06 || y>0.76 || y<0.28)) {
					for(double i=0.06;i<0.75;i+=0.07777) {
						for(double j=0.28;j<0.75;j+=0.096) {
							if((i<=x && x<i+0.07777) && (j<=y && y<j+0.096)) {
								grille.get(j2).set(i2, new Pair<>((Plante) e, grille.get(j2).get(i2).getValue()));
							}
							j2++;
						}
						j2=0;
						i2++;
					}
				}
			}
		}else {
			int i2 = 0;
			int j2 = 0;
			if(!(x>0.76 || x<0.06 || y>0.76 || y<0.28)) {
				for(double i=0.06;i<0.75;i+=0.07777) {
					for(double j=0.28;j<0.75;j+=0.096) {
						if((i<=x && x<i+0.07777) && (j<=y && y<j+0.096)) {
							grille.get(j2).get(i2).getValue().add((Zombie) e);
						}
						j2++;
					}
					j2=0;
					i2++;
				}
			}
		}
	}
	
	/**
	 * supprime une plante dans la grille à la position(x,y)
	 * avec x,y compris dans la grille
	 * 
	 * @param x un double coordonné x
	 * @param y un double coordonné y
	 * 
	 */
	
	public void supprimePlante(double x, double y) {
		int i2 = 0;
		int j2 = 0;
		if(!(x>0.76 || x<0.06 || y>0.76 || y<0.28)) {
			for(double i=0.06;i<0.75;i+=0.07777) {
				for(double j=0.28;j<0.75;j+=0.096) {
					if((i<=x && x<i+0.07777) && (j<=y && y<j+0.096)) {
						grille.get(j2).set(i2, new Pair<>(null, grille.get(j2).get(i2).getValue()));
					}
					j2++;
				}
				j2=0;
				i2++;
			}
		}
	}
	
	/**
	 * supprime un zombie à tout les endroits de la grille
	 * avec x,y compris dans la grille
	 * on supprime le zombie de tout les endroits où il se trouve 
	 * 
	 * @param z un zombie
	 * 
	 */
	
	public void supprimeZombieP(Zombie z) {
		double x = z.getX();
		double y = z.getY();
		int i2 = 0;
		int j2 = 0;
		int id = z.getID();
		int f=0;
		if(!(x>0.76 || x<0.06 || y>0.76 || y<0.28)) {
			for(double i=0.06;i<0.75;i+=0.07777) {
				for(double j=0.28;j<0.75;j+=0.096) {
					f=0;
					while(getZombie(i,j,f) != null) {
						if(id==getZombie(i,j,f).getID()) {
							grille.get(j2).get(i2).getValue().remove(f);
						}
						f++;
					}
					j2++;
				}
				j2=0;
				i2++;
			}
		}
	}
	
	/**
	 * détecte si une plante est dans la grille à la position(x,y)
	 * avec x,y compris dans la grille
	 * rend vrai si il y a une plante
	 * 
	 * @param x un double coordonné x
	 * @param y un double coordonné y
	 * 
	 * @return boolean true si il y a une plante
	 */
	
	public boolean collisionPlante(double x, double y) {
		int i2 = 0;
		int j2 = 0;
		if(!(x>0.76 || x<0.06 || y>0.76 || y<0.28)) {
			for(double i=0.06;i<0.75;i+=0.07777) {
				for(double j=0.28;j<0.75;j+=0.096) {
					if((i<=x && x<i+0.07777) && (j<=y && y<j+0.096)) {
						if(grille.get(j2).get(i2).getKey() != null) {
							return true;
						}
					}
					j2++;
				}
				j2=0;
				i2++;
			}
		}
		return false;
	}
	
	/**
	 * détecte si un zombie est dans la grille à la position(x,y)
	 * avec x,y compris dans la grille
	 * rend vrai si il y a un zombie
	 * 
	 * @param x un double coordonné x
	 * @param y un double coordonné y
	 * 
	 * @return boolean 
	 */
	
	public boolean collisionZombie(double x, double y) {
		int i2 = 0;
		int j2 = 0;
		if(!(x>0.76 || x<0.06 || y>0.76 || y<0.28)) {
			for(double i=0.06;i<0.75;i+=0.07777) {
				for(double j=0.28;j<0.75;j+=0.096) {
					if((i<=x && x<i+0.07777) && (j<=y && y<j+0.096)) {
						if(!(grille.get(j2).get(i2).getValue().isEmpty())) {
							return true;
						}
					}
					j2++;
				}
				j2=0;
				i2++;
			}
		}
		return false;
	}
	
	/**
	 * donne une plante qui est dans la grille à la position(x,y)
	 * avec x,y compris dans la grille
	 * 
	 * @param x un double coordonné x
	 * @param y un double coordonné y
	 * 
	 * @return Plante la plante à la position demandé
	 */
	
	public Plante getPlante(double x, double y) {
		int i2 = 0;
		int j2 = 0;
		if(!(x>0.76 || x<0.06 || y>0.76 || y<0.28)) {
			for(double i=0.06;i<0.75;i+=0.07777) {
				for(double j=0.28;j<0.75;j+=0.096) {
					if((i<=x && x<i+0.07777) && (j<=y && y<j+0.096)) {
						return grille.get(j2).get(i2).getKey();
					}
					j2++;
				}
				j2=0;
				i2++;
			}
		}
		return null;
	}
	
	/**
	 * donne un zombie qui est dans la grille à la position(x,y)
	 * avec x,y compris dans la grille
	 * permet d'avoir le zombie à la place f de la liste
	 * 
	 * @param x un double coordonné x
	 * @param y un double coordonné y
	 * @param f un int 
	 * 
	 * @return Entite le zombie à la place f dans la liste à la position demandé
	 */
	
	public Zombie getZombie(double x, double y, int f) {
		int i2 = 0;
		int j2 = 0;
		if(!(x>0.76 || x<0.06 || y>0.76 || y<0.28)) {
			for(double i=0.06;i<0.75;i+=0.07777) {
				for(double j=0.28;j<0.75;j+=0.096) {
					if((i<=x && x<i+0.07777) && (j<=y && y<j+0.096)) {
						if(grille.get(j2).get(i2).getValue().isEmpty()) {
							return null;
						}
						if(grille.get(j2).get(i2).getValue().size()-1 < f) {
							return null;
						}
					return grille.get(j2).get(i2).getValue().get(f);
					}
					j2++;
				}
				j2=0;
				i2++;
			}
		}
		return null;
	}
	
	/**
	 * donne un zombie qui est dans la grille à la position(x,y)
	 * avec x,y compris dans la grille
	 * 
	 * @param x un double coordonné x
	 * @param y un double coordonné y
	 * 
	 * @return Entite le premier zombie à la position demandé
	 */
	public Zombie tapeZombie(double x, double y) {
		int i2 = 0;
		int j2 = 0;
		if(!(x>0.76 || x<0.06 || y>0.76 || y<0.28)) {
			for(double i=0.06;i<0.75;i+=0.07777) {
				for(double j=0.28;j<0.75;j+=0.096) {
					if((i<=x && x<i+0.07777) && (j<=y && y<j+0.096)) {
						if(grille.get(j2).get(i2).getValue().isEmpty()) {
							return null;
						}
					return grille.get(j2).get(i2).getValue().get(0);
					}
					j2++;
				}
				j2=0;
				i2++;
			}
		}
		return null;
	}
	
	
	
	
	@Override
	public String toString() {
		String str="";
		for(int j=0;j<9;j++) {
			for(int i=0;i<5;i++) {
				str += " " +grille.get(i).get(j);
			}
		}
		return str;
	}
	
}
