

public class ZombieBase extends Zombie{

	private int vie;
	private int degat;
	
	private double TRUC_MOVE_X = 0.00038; // 0.25 case par secondes
	private static final double TRUC_SIZE = 0.17;
	private boolean statut;
	private Timer timerAttaque;
	private boolean fin;
	
	
	public ZombieBase(double x, double y, int id) {
		super(x,y,id);
		this.vie = 200;
		this.degat = 30;
		
		this.statut = true;
		timerAttaque = new Timer(1000);
		this.fin = false;
	}

	public int getVie() {
		return vie;
	}



	public void setVie(int vie) {
		this.vie = vie;
	}



	public int getDegat() {
		return degat;
	}



	public void setDegat(int degat) {
		this.degat = degat;
	}
	
	public double getTRUC_MOVE_X() {
		return TRUC_MOVE_X;
	}

	public void setTRUC_MOVE_X(double tRUC_MOVE_X) {
		TRUC_MOVE_X = tRUC_MOVE_X;
	}

	@Override
	public void step() {
		if (getVie()<=0) {
			this.fin = true; // on change le statut
		}
		ZombieBase z = new ZombieBase(this.position.getX(), this.position.getY(), getID());
		z.setVie(getVie());
			//si le zombie arrive devant une plante il s'arrête et l'attaque
			//sinon il avance
			if(GameWorld.grille.collisionPlante(this.position.getX(), this.position.getY()) && 
					!(GameWorld.grille.getPlante(this.position.getX(), this.position.getY()) instanceof Cerise)) {
				this.statut = false;
				if (timerAttaque.hasFinished()) {
					if(GameWorld.grille.getPlante(this.position.getX(), this.position.getY()) instanceof TirePois) {
						((TirePois) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).setVie(((TirePois) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).getVie() - getDegat());
					}else if(GameWorld.grille.getPlante(this.position.getX(), this.position.getY()) instanceof Tournesol) {
						((Tournesol) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).setVie(((Tournesol) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).getVie() - getDegat());
					}else if(GameWorld.grille.getPlante(this.position.getX(), this.position.getY()) instanceof Noix) {
						((Noix) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).setVie(((Noix) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).getVie() - getDegat());
					}else if(GameWorld.grille.getPlante(this.position.getX(), this.position.getY()) instanceof Boxe) {
						((Boxe) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).setVie(((Boxe) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).getVie() - getDegat());
					}else if(GameWorld.grille.getPlante(this.position.getX(), this.position.getY()) instanceof Carni) {
						((Carni) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).setVie(((Carni) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).getVie() - getDegat());
					}else if(GameWorld.grille.getPlante(this.position.getX(), this.position.getY()) instanceof Champi) {
						((Champi) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).setVie(((Champi) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).getVie() - getDegat());
					}else if(GameWorld.grille.getPlante(this.position.getX(), this.position.getY()) instanceof TirePoisGele) {
						((TirePoisGele) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).setVie(((TirePoisGele) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).getVie() - getDegat());
					}else if(GameWorld.grille.getPlante(this.position.getX(), this.position.getY()) instanceof TirePoisFeu) {
						((TirePoisFeu) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).setVie(((TirePoisFeu) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).getVie() - getDegat());
					}else if(GameWorld.grille.getPlante(this.position.getX(), this.position.getY()) instanceof TirePoisBoss) {
						((TirePoisBoss) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).setVie(((TirePoisBoss) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).getVie() - getDegat());
					}
					timerAttaque.restart();
				}
			}else {
				this.statut = true;
				//on fais bouger le zombie dans la grille à la même vitesse que son déplacement
				GameWorld.grille.supprimeZombieP(z);
				GameWorld.grille.ajoute(z, this.position.getX(), this.position.getY());
				this.position.setX(this.position.getX() - TRUC_MOVE_X);
			}
			if(fin) {
				GameWorld.grille.supprimeZombieP(z);
				GameWorld.entites.set(getID(),null);
			}
		}
	

	@Override
	public void dessine() {
			if(!fin) {
				if (!statut)
					StdDraw.picture(this.position.getX(), this.position.getY()-0.025, "image/zmanger.gif", TRUC_SIZE-0.07, TRUC_SIZE-0.05);
				else	
					StdDraw.picture(this.position.getX(), this.position.getY(), "image/z.gif", TRUC_SIZE, TRUC_SIZE);
			}
		}
	

	@Override
	public String toString() {
		return super.toString() + " Zombie de Base [vie=" + vie + ", degat=" + degat + "]";
	}
	
	
	
}
