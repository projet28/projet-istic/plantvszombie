public class Cerise extends Plante{
	private int vie;
	private int degats;
	
	private static final double TRUC_SIZE = 0.15;
	private boolean statut;
	private boolean explose;
	
	private Timer att;
	private Timer exp;
	
	public Cerise(double x, double y, int id) {
		super(x,y,id);
		this.vie = 1;
		this.degats = 500;
		
		this.statut = true;
		this.explose = false;
		
		this.att = new Timer(10000);
		this.exp = new Timer(2000);
	}

	public int getVie() {
		return vie;
	}

	public void setVie(int vie) {
		this.vie = vie;
	}
	
	public int getDegats() {
		return degats;
	}

	public void setDegats(int degats) {
		this.degats = degats;
	}
	
	@Override
	public void step() {
		
		if (exp.hasFinished()) {
			this.statut = false; // on change le statut
		}
		
		if(getVie() >0) {
			exp.restart();
		}else {
			GameWorld.grille.supprimePlante(this.position.getX(), this.position.getY());
		}
		
		if(GameWorld.grille.collisionZombie(this.position.getX(), this.position.getY()) && att.hasFinished() && !explose) {
			int i=0;
			for(double x=this.position.getX()-0.0777;x<=this.position.getX()+0.0777;x+=0.0777) {
				for(double y=this.position.getY()-0.096;y<=this.position.getY()+0.096;y+=0.096) {
					while(GameWorld.grille.getZombie(x, y, i)!=null) {
						if(GameWorld.grille.getZombie(x, y,i) instanceof ZombieBase){
							((ZombieBase) GameWorld.entites.get(GameWorld.grille.getZombie(x, y,i).getID())).setVie(((ZombieBase) GameWorld.entites.get(GameWorld.grille.getZombie(x, y,i).getID())).getVie() - getDegats());
						}else if(GameWorld.grille.getZombie(x, y,i) instanceof ZombieBlinde) {
							((ZombieBlinde) GameWorld.entites.get(GameWorld.grille.getZombie(x, y,i).getID())).setVie(((ZombieBlinde) GameWorld.entites.get(GameWorld.grille.getZombie(x, y,i).getID())).getVie() - getDegats());
						}else if(GameWorld.grille.getZombie(x, y,i) instanceof ZombieBoss) {
							((ZombieBoss) GameWorld.entites.get(GameWorld.grille.getZombie(x, y,i).getID())).setVie(((ZombieBoss) GameWorld.entites.get(GameWorld.grille.getZombie(x, y,i).getID())).getVie() - getDegats());
						}
						i++;
					}
					i=0;
				}
			}
			setVie(0);
			explose = true;
		}
		
		if (!statut) {
			GameWorld.entites.set(getID(), null);
		}
	}

	@Override
	public void dessine() {
		if (statut) {
			if(!explose) {
				StdDraw.picture(this.position.getX(), this.position.getY()-0.05, "image/cerise.GIF", TRUC_SIZE, TRUC_SIZE);
			}
		}
		if(explose && !exp.hasFinished()) {
			StdDraw.picture(this.position.getX(), this.position.getY()-0.05, "image/ceriseexplose.gif", TRUC_SIZE, TRUC_SIZE);
		}
	}


	@Override
	public String toString() {
		return super.toString() + " Cerise [vie=" + vie + "]";
	}
}
