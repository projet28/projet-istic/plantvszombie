
public class Carni extends Plante{
	private int vie;
	private int degats;
	
	private static final double TRUC_SIZE = 0.1;
	private boolean statut;
	private boolean statutTir;
	
	private Timer att;
	private Timer anim;
	
	public Carni(double x, double y, int id) {
		super(x,y,id);
		this.vie = 500;
		this.degats = 9999;
		
		this.statut = true;
		this.statutTir = false;
		
		this.att = new Timer(2000);
		this.anim = new Timer(2000);
	}

	public int getVie() {
		return vie;
	}

	public void setVie(int vie) {
		this.vie = vie;
	}
	
	public int getDegats() {
		return degats;
	}

	public void setDegats(int degats) {
		this.degats = degats;
	}

	@Override
	public void step() {
		
		if (getVie()<=0) {
			this.statut = false; // on change le statut
		}
		if(GameWorld.grille.collisionZombie(this.position.getX(), this.position.getY())) {
			if(att.hasFinished()) {
				statutTir = true;
				if(GameWorld.grille.tapeZombie(this.position.getX(), this.position.getY()) instanceof ZombieBase){
					((ZombieBase) GameWorld.entites.get(GameWorld.grille.tapeZombie(this.position.getX(), this.position.getY()).getID())).setVie(((ZombieBase) GameWorld.entites.get(GameWorld.grille.tapeZombie(this.position.getX(), this.position.getY()).getID())).getVie() - getDegats());
				}else if(GameWorld.grille.tapeZombie(this.position.getX(), this.position.getY()) instanceof ZombieBlinde) {
					((ZombieBlinde) GameWorld.entites.get(GameWorld.grille.tapeZombie(this.position.getX(), this.position.getY()).getID())).setVie(((ZombieBlinde) GameWorld.entites.get(GameWorld.grille.tapeZombie(this.position.getX(), this.position.getY()).getID())).getVie() - getDegats());
				}
				att.restart();
			}
		}else if(GameWorld.grille.collisionZombie(this.position.getX()+0.0777, this.position.getY())) {
			if(att.hasFinished()) {
				statutTir = true;
				if(GameWorld.grille.tapeZombie(this.position.getX()+0.0777, this.position.getY()) instanceof ZombieBase){
					((ZombieBase) GameWorld.entites.get(GameWorld.grille.tapeZombie(this.position.getX()+0.0777, this.position.getY()).getID())).setVie(((ZombieBase) GameWorld.entites.get(GameWorld.grille.tapeZombie(this.position.getX()+0.0777, this.position.getY()).getID())).getVie() - getDegats());
				}else if(GameWorld.grille.tapeZombie(this.position.getX()+0.0777, this.position.getY()) instanceof ZombieBlinde) {
					((ZombieBlinde) GameWorld.entites.get(GameWorld.grille.tapeZombie(this.position.getX()+0.0777, this.position.getY()).getID())).setVie(((ZombieBlinde) GameWorld.entites.get(GameWorld.grille.tapeZombie(this.position.getX()+0.0777, this.position.getY()).getID())).getVie() - getDegats());
				}
				att.restart();
			}
		}else{
			if(anim.hasFinished()) {
				statutTir = false;
				anim.restart();
			}
			att.restart();
		}
		
		if (!statut) {
			GameWorld.grille.supprimePlante(this.position.getX(), this.position.getY());
			GameWorld.entites.set(getID(), null);
		}
	}

	@Override
	public void dessine() {
		if (statutTir) {
			if(!anim.hasFinished()) {
				StdDraw.picture(this.position.getX(), this.position.getY()-0.05, "image/carnimange.GIF", TRUC_SIZE, TRUC_SIZE);
			}
		}else {
			StdDraw.picture(this.position.getX(), this.position.getY()-0.05, "image/carni.GIF", TRUC_SIZE, TRUC_SIZE);
			anim.restart();
		}
	}


	@Override
	public String toString() {
		return super.toString() + " Carni [vie=" + vie + "]";
	}
}
