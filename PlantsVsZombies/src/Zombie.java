
public class Zombie extends Entite{
	
	private int ID;
	
	public Zombie(double x, double y, int id) {
		super(x,y);
		this.ID = id;
	}

	public int getID() {
		return ID;
	}

	public void setID(int id) {
		this.ID = id;
	}
	
	@Override
	public void step() {
		this.position.setX(this.position.getX());
	}

	@Override
	public void dessine() {
	}

	@Override
	public String toString() {
		return "";
	}
}
