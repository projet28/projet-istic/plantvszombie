
public class Champi extends Plante{
	private int vie;
	
	private static final double TRUC_SIZE = 0.1;
	private boolean statut;
	
	private Timer att;
	
	public Champi(double x, double y, int id) {
		super(x,y,id);
		this.vie = 300;
		
		this.statut = true;
		this.att = new Timer(2000);
	}

	public int getVie() {
		return vie;
	}



	public void setVie(int vie) {
		this.vie = vie;
	}
	
	@Override
	public void step() {
		
		//si la plante n'a plus de vie on change le statut pour la faire disparaitre de la grille
		if (getVie()<=0) {
			this.statut = false; // on change le statut
		}
		if(att.hasFinished()) {
			for(double i=this.position.getX();i<=0.76;i+=0.07777) {
				if(GameWorld.grille.collisionZombie(i, this.position.getY())) {
					if(GameWorld.grille.tapeZombie(i, this.position.getY()) instanceof ZombieBase){
						((ZombieBase) GameWorld.entites.get(GameWorld.grille.tapeZombie(i, this.position.getY()).getID())).setVie(((ZombieBase) GameWorld.entites.get(GameWorld.grille.tapeZombie(i, this.position.getY()).getID())).getVie() - 10);
					}else if(GameWorld.grille.tapeZombie(i, this.position.getY()) instanceof ZombieBlinde) {
						((ZombieBlinde) GameWorld.entites.get(GameWorld.grille.tapeZombie(i, this.position.getY()).getID())).setVie(((ZombieBlinde) GameWorld.entites.get(GameWorld.grille.tapeZombie(i, this.position.getY()).getID())).getVie() - 10);
					}else if(GameWorld.grille.tapeZombie(i, this.position.getY()) instanceof ZombieBoss) {
						((ZombieBoss) GameWorld.entites.get(GameWorld.grille.tapeZombie(i, this.position.getY()).getID())).setVie(((ZombieBoss) GameWorld.entites.get(GameWorld.grille.tapeZombie(i, this.position.getY()).getID())).getVie() - 10);
					}
				}
			}
			att.restart();
		}
		if (!statut) {
			GameWorld.grille.supprimePlante(this.position.getX(), this.position.getY());
			GameWorld.entites.set(getID(), null);
		}
	}

	@Override
	public void dessine() {
		if (statut) {
			StdDraw.picture(this.position.getX(), this.position.getY()-0.05, "image/champi.GIF", TRUC_SIZE, TRUC_SIZE);
			for(double i=this.position.getX();i<0.75;i+=0.07777) {
				StdDraw.picture(i, this.position.getY()-0.05, "image/poison.GIF", TRUC_SIZE, TRUC_SIZE);
			}
		}
	}

	@Override
	public String toString() {
		return super.toString() + " TirePois [vie=" + vie + "]";
	}
}
