
public class ZombieBoss extends Zombie{

	private int vie;
	private int degat;
	
	private double TRUC_MOVE_X; 
	private static double TRUC_SIZE;
	private boolean statut;
	private boolean mort;
	private Timer timerAttaque;
	
	private Timer baisse;
	
	public ZombieBoss(double x, double y, int id) {
		super(x,y,id);
		this.vie = 2000;
		this.degat = 100;
		
		TRUC_MOVE_X = 0.00074;
		TRUC_SIZE = 0.2;
		
		this.statut = true;
		this.mort = false;
		timerAttaque = new Timer(1000);
		
		this.baisse = new Timer(2000);
	}

	public int getVie() {
		return vie;
	}


	public void setVie(int vie) {
		this.vie = vie;
	}


	public int getDegat() {
		return degat;
	}


	public void setDegat(int degat) {
		this.degat = degat;
	}
	
	public double getTRUC_MOVE_X() {
		return TRUC_MOVE_X;
	}

	public void setTRUC_MOVE_X(double tRUC_MOVE_X) {
		TRUC_MOVE_X = tRUC_MOVE_X;
	}
	
	/**
	 * détecte si une plante est dans la colonne du zombie
	 * avec x la position x du zombie
	 * rend faux si il y a une plante
	 * 
	 * @param x un double coordonné x
	 * 
	 * @return boolean false si il y a une plante sur la colonne du zombie
	 */
	public boolean deplaceX(double x) {
		for(double j=0.3;j<0.75;j+=0.096) {
			if(GameWorld.grille.collisionPlante(this.position.getX(), j)) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * détecte si il y a une plante au dessus et en dessous du zombie
	 * avec x,y la position x du zombie
	 * rend vrai si il y a une plante au dessus et en dessous du zombie
	 * 
	 * @param x un double coordonné x
	 * @param y un double coordonné y
	 * 
	 * @return boolean true si il y a une plante sur la colonne du zombie en dessous et au dessus de lui
	 */
	public static boolean doubleDir(double x, double y) {
		boolean bool1 = false;
		for(double j=0.288;j<y;j+=0.096) {
			if(GameWorld.grille.collisionPlante(x, j)) {
				bool1=true;;
			}
		}
		boolean bool2 = false;
		for(double j=y+0.043;j<1;j+=0.096) {
			if(GameWorld.grille.collisionPlante(x, j)) {
				bool2=true;
			}
		}
		return (bool1 && bool2);
	}
	
	/**
	 * utilise les 2 fonctions précédente pour savoir si le zombie doit aller attaquer en haut en bas ou continuer à avancer
	 * sur l'axe x 
	 * 
	 * @param y un double coordonné y
	 * 
	 */
	public void bouge(double y, boolean dir) {
		ZombieBoss z = new ZombieBoss(this.position.getX(), this.position.getY(), getID());
		z.setVie(getVie());
		GameWorld.grille.supprimeZombieP(z);
		GameWorld.grille.ajoute(z, this.position.getX(), this.position.getY());
		this.position.setX(this.position.getX() - TRUC_MOVE_X/10);
		
		if(doubleDir(this.position.getX(), y)) {
			GameWorld.grille.supprimeZombieP(z);
			GameWorld.grille.ajoute(z, this.position.getX(), this.position.getY());
			this.position.setY(this.position.getY() + (TRUC_MOVE_X*2));
		}else if(deplaceX(this.position.getX())) {
			GameWorld.grille.supprimeZombieP(z);
			GameWorld.grille.ajoute(z, this.position.getX(), this.position.getY());
			this.position.setX(this.position.getX() - TRUC_MOVE_X);
		}else {
			for(double j=y+0.096;j<=1;j+=0.096) {
				if(GameWorld.grille.collisionPlante(this.position.getX(), j)) {
					GameWorld.grille.supprimeZombieP(z);
					GameWorld.grille.ajoute(z, this.position.getX(), this.position.getY());
					this.position.setY(this.position.getY() + TRUC_MOVE_X);
				}
			}
			for(double j=y-0.096;j>=0;j-=0.096) {
				if(GameWorld.grille.collisionPlante(this.position.getX(), j)) {
					GameWorld.grille.supprimeZombieP(z);
					GameWorld.grille.ajoute(z, this.position.getX(), this.position.getY());
					this.position.setY(this.position.getY() - TRUC_MOVE_X);
				}
			}
		}
	}
	
	@Override
	public void step() {
		ZombieBoss z = new ZombieBoss(this.position.getX(), this.position.getY(), getID());
		z.setVie(getVie());
		if(!mort) {
			if (getVie()<=0) {
				this.mort = true; // on change le statut
			}
			
			//si le zombie arrive devant une plante il s'arrête et l'attaque
			//sinon il avance
			if(GameWorld.grille.collisionPlante(this.position.getX(), this.position.getY())
					&& !(GameWorld.grille.getPlante(this.position.getX(), this.position.getY()) instanceof Cerise)) {
				
				this.statut = false;
				if (timerAttaque.hasFinished()) {
					if(GameWorld.grille.getPlante(this.position.getX(), this.position.getY()) instanceof TirePois) {
						((TirePois) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).setVie(((TirePois) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).getVie() - getDegat());
					}else if(GameWorld.grille.getPlante(this.position.getX(), this.position.getY()) instanceof Tournesol) {
						((Tournesol) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).setVie(((Tournesol) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).getVie() - getDegat());
					}else if(GameWorld.grille.getPlante(this.position.getX(), this.position.getY()) instanceof Noix) {
						((Noix) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).setVie(((Noix) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).getVie() - getDegat());
					}else if(GameWorld.grille.getPlante(this.position.getX(), this.position.getY()) instanceof Boxe) {
						((Boxe) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).setVie(((Boxe) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).getVie() - getDegat());
					}else if(GameWorld.grille.getPlante(this.position.getX(), this.position.getY()) instanceof Carni) {
						((Carni) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).setVie(((Carni) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).getVie() - getDegat());
					}else if(GameWorld.grille.getPlante(this.position.getX(), this.position.getY()) instanceof Champi) {
						((Champi) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).setVie(((Champi) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).getVie() - getDegat());
					}else if(GameWorld.grille.getPlante(this.position.getX(), this.position.getY()) instanceof TirePoisGele) {
						((TirePoisGele) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).setVie(((TirePoisGele) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).getVie() - getDegat());
					}else if(GameWorld.grille.getPlante(this.position.getX(), this.position.getY()) instanceof TirePoisFeu) {
						((TirePoisFeu) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).setVie(((TirePoisFeu) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).getVie() - getDegat());
					}else if(GameWorld.grille.getPlante(this.position.getX(), this.position.getY()) instanceof TirePoisBoss) {
						((TirePoisBoss) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).setVie(((TirePoisBoss) GameWorld.grille.getPlante(this.position.getX(), this.position.getY())).getVie() - getDegat());
					}
					timerAttaque.restart();
				}
			}else {
				this.statut = true;
				
				//on fais bouger le zombie dans la grille à la même vitesse que son déplacement
				
				bouge(this.position.getY(), true);
			}
		}
		if(mort) {
			GameWorld.grille.supprimeZombieP(z);
			GameWorld.entites.set(getID(),null);
		}
	}

	@Override
	public void dessine() {
		if(!mort) {
			if(!statut && !baisse.hasFinished()) {
				StdDraw.picture(this.position.getX(), this.position.getY()-0.05, "image/zbossbaisse.gif", TRUC_SIZE, TRUC_SIZE);
			}else if (!statut && baisse.hasFinished()) {
				StdDraw.picture(this.position.getX(), this.position.getY()-0.05, "image/zbossmange.gif", TRUC_SIZE-0.05, TRUC_SIZE-0.05);
			}else{
				StdDraw.picture(this.position.getX(), this.position.getY()-0.05, "image/zboss.GIF", TRUC_SIZE, TRUC_SIZE);
				baisse.restart();
			}
		}
	}

	@Override
	public String toString() {
		return super.toString() + " Zombie Boss [vie=" + vie + ", degat=" + degat + "]";
	}
}
