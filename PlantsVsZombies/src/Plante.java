
public class Plante extends Entite{

	private int ID;
	
	public Plante(double x, double y, int id) {
		super(x,y);
		this.ID = id;
	}

	public int getID() {
		return ID;
	}

	public void setID(int ID) {
		this.ID = ID;
	}
	
	@Override
	public void step() {
		this.position.setX(this.position.getX());
	}

	@Override
	public void dessine() {
	}

	@Override
	public String toString() {
		return "La plante ";
	}
}
