


public class Main {
	//Les variables pour les musiques mp3
	static Sound musique = null;
	static Runner runMenu = null;
	static Stopper stopMenu = null;
	static Runner run1 = null;
	static Stopper stop1 = null;
	static Runner run2 = null;
	static Stopper stop2 = null;
	static Runner run3 = null;
	static Stopper stop3 = null;
	static Runner run4 = null;
	static Stopper stop4 = null;
	static Runner run5 = null;
	static Stopper stop5 = null;
	static Runner run6 = null;
	static Stopper stop6 = null;
	static Runner run7 = null;
	static Stopper stop7 = null;
	static Runner run8 = null;
	static Stopper stop8 = null;
	static Runner run9 = null;
	static Stopper stop9 = null;
	static Runner run10 = null;
	static Stopper stop10 = null;
	static Runner runS = null;
	static Stopper stopS = null;
	static Runner runC = null;
	static Stopper stopC = null;
	static Runner runW = null;
	static Stopper stopW = null;
	static Runner runL = null;
	static Stopper stopL = null;
	
	static Timer replayMenu = new Timer(170000);
	static Timer replay1 = new Timer(207000);
	static Timer replay2 = new Timer(408000);
	static Timer replay3 = new Timer(18700);
	static Timer replay4 = new Timer(188000);
	static Timer replay5 = new Timer(300000);
	static Timer replay6 = new Timer(155000);
	static Timer replay7 = new Timer(213000);
	static Timer replay8 = new Timer(182000);
	static Timer replay9 = new Timer(380000);
	static Timer replay10 = new Timer(260000);
	static Timer replayS = new Timer(870000);
	static Timer replayC = new Timer(262000);
	
	
	//Les variables pour les musiques wav
		static boolean transition = false;
		
	private static String str="";
	private static Clavier clavier = new Clavier();
	
	public static Compte compte(boolean nouv) {
		char lettre = '\0';
		while(lettre!='\n') {
			StdDraw.clear();
			StdDraw.picture(0.5, 0.5, "image/menu/wallpaper.jpg",1,1);
			StdDraw.picture(0.5, 0.75, "image/menu/logo.png",0.4,0.25);
			StdDraw.setPenColor(StdDraw.PRINCETON_ORANGE);
			StdDraw.filledRectangle(0.5, 0.43, 0.15, 0.1);
			if(nouv) {
				StdDraw.setPenColor(StdDraw.BLUE);
				StdDraw.text(0.5, 0.45, "Votre nom de compte : ");
			}else {
				StdDraw.setPenColor(StdDraw.BLUE);
				StdDraw.text(0.5, 0.45, "Quel est votre pseudo? : ");
			}
			
			if (StdDraw.hasNextKeyTyped() && str.length()<15) {
				char key = StdDraw.nextKeyTyped();
				lettre = clavier.processUserInput(key);
				if(lettre == ' ' && str.length()>0) {
					str = str.substring(0, str.length()-1);
				}else if(lettre!='\n') str+=lettre;
			}
			
			if(str.length()==15) {
				if (StdDraw.hasNextKeyTyped()) {
					char key = StdDraw.nextKeyTyped();
					lettre = clavier.processUserInput(key);
					if(lettre == ' ' && str.length()>1) {
						str = str.substring(0, str.length()-1);
					}
				}
			}
			
			StdDraw.setPenColor(StdDraw.BLACK);
			StdDraw.filledRectangle(0.5, 0.4, 0.12, 0.03);
			StdDraw.setPenColor(StdDraw.PRINCETON_ORANGE);
			StdDraw.text(0.5, 0.4, str);
			
			StdDraw.show();
			StdDraw.pause(20);
		}
		
		Compte compte = null;
			//demander si il veut un charger un compte ou en créer un
			if(nouv) {
				compte = new Compte(str,nouv);
			}else {
				compte = new Compte(str,nouv);
			}
		return compte;
	}
	
	public static void niveau(Compte compte, int i) {
		if(i==99) {
			GameWorld.niveau = i;
		}else if(compte.getNiveau()>=i) {
			GameWorld.niveau = i;
		}
	}
	
	private static boolean menu = true;
	private static boolean bool = false;
	static boolean music = true;
	private static int i = 0;
	private static Timer touche = new Timer(100);
	private static Chrono survie = new Chrono();
	private static Compte compte = null;
	private static Compte classement = new Compte("classement", false);
	private static int niv = 0;
	
	public static void Menu() {
		if(i==0) {
			StdDraw.picture(0.5, 0.5, "image/menu/wallpaper.jpg",1,1);
			StdDraw.picture(0.5, 0.75, "image/menu/logo.png",0.4,0.25);
			StdDraw.picture(0.5, 0.5, "image/menu/BoutonNouveauCompte.png");
			StdDraw.picture(0.5, 0.3, "image/menu/BoutonChargerCompte.png");
		}
		if(i==1) {
			StdDraw.picture(0.5, 0.5, "image/menu/wallpaper.jpg",1,1);
			StdDraw.picture(0.5, 0.75, "image/menu/logo.png",0.4,0.25);
			StdDraw.picture(0.5, 0.5, "image/menu/BoutonCompte.png");
			StdDraw.picture(0.5, 0.4, "image/menu/BoutonNiveaux.png");
			StdDraw.picture(0.5, 0.3, "image/menu/BoutonClassement.png");
			StdDraw.picture(0.5, 0.2, "image/menu/BoutonRetour.png");
		}
		if(i==2) {
			StdDraw.picture(0.5, 0.5, "image/menu/wallpaper.jpg",1,1);
			StdDraw.picture(0.5, 0.75, "image/menu/logo.png",0.4,0.25);
			StdDraw.picture(0.1, 0.5, "image/menu/Niveau 1.png");
			StdDraw.picture(0.3, 0.5, "image/menu/Niveau 2.png");
			StdDraw.picture(0.5, 0.5, "image/menu/Niveau 3.png");
			StdDraw.picture(0.7, 0.5, "image/menu/Niveau 4.png");
			StdDraw.picture(0.9, 0.5, "image/menu/Niveau 5.png");
			StdDraw.picture(0.1, 0.4, "image/menu/Niveau 6.png");
			StdDraw.picture(0.3, 0.4, "image/menu/Niveau 7.png");
			StdDraw.picture(0.5, 0.4, "image/menu/Niveau 8.png");
			StdDraw.picture(0.7, 0.4, "image/menu/Niveau 9.png");
			StdDraw.picture(0.9, 0.4, "image/menu/Niveau 10.png");
			StdDraw.picture(0.5, 0.3, "image/menu/BoutonSurvie.png");
			StdDraw.picture(0.5, 0.2, "image/menu/BoutonRetour.png");
		}
		if(i==3) {
			StdDraw.picture(0.5, 0.5, "image/menu/wallpaper.jpg",1,1);
			StdDraw.picture(0.5, 0.75, "image/menu/logo.png",0.4,0.25);
			StdDraw.setPenColor(StdDraw.PRINCETON_ORANGE);
			StdDraw.filledRectangle(0.5, 0.43, 0.15, 0.2);
			String[] tab = classement.getClassement();
			int j = 1;
			while(j<20) {
				StdDraw.setPenColor(StdDraw.BLUE);
				StdDraw.text(0.5, 0.64-((double) j/50), tab[j-1]);
				StdDraw.setPenColor(StdDraw.BOOK_BLUE);
				StdDraw.text(0.5, 0.62-((double) j/50), tab[j]);
				j+=2;
			}
			
			StdDraw.picture(0.5, 0.2, "image/menu/BoutonRetour.png");
		}
		if(i==4) {
			StdDraw.picture(0.5, 0.5, "image/menu/wallpaper.jpg",1,1);
			StdDraw.picture(0.5, 0.75, "image/menu/logo.png",0.4,0.25);
			StdDraw.setPenColor(StdDraw.PRINCETON_ORANGE);
			StdDraw.filledRectangle(0.5, 0.43, 0.15, 0.2);
			StdDraw.setPenColor(StdDraw.BLUE);
			StdDraw.text(0.5, 0.54, "niveau actuelle :");
			StdDraw.setPenColor(StdDraw.BOOK_BLUE);
			if(compte.getNiveau() == 11) {
				StdDraw.text(0.5, 0.44, "fini");
			}else {
				StdDraw.text(0.5, 0.44, Integer.toString(compte.getNiveau()));
			}
			StdDraw.setPenColor(StdDraw.BLUE);
			StdDraw.text(0.5, 0.4, "meilleur score en survie :");
			StdDraw.setPenColor(StdDraw.BOOK_BLUE);
			StdDraw.text(0.5, 0.3, compte.getScore());
			StdDraw.picture(0.5, 0.2, "image/menu/BoutonRetour.png");
		}
		if(i==5) {
			StdDraw.picture(0.5, 0.5, "image/menu/wallpaper.jpg",1,1);
			StdDraw.picture(0.5, 0.75, "image/menu/logo.png",0.4,0.25);
			StdDraw.picture(0.5, 0.4, "image/menu/FFXV.gif");
			StdDraw.picture(0.5, 0.4, "image/menu/BoutonNiveauSuivant.png");
			StdDraw.picture(0.5, 0.2, "image/menu/BoutonRetour.png");
		}
		if(i==6) {
			StdDraw.picture(0.5, 0.5, "image/menu/wallpaper.jpg",1,1);
			StdDraw.picture(0.5, 0.75, "image/menu/logo.png",0.4,0.25);
			StdDraw.picture(0.5, 0.4, "image/menu/naruto.gif");
			StdDraw.picture(0.5, 0.4, "image/menu/BoutonRecommencerNiveau.png");
			StdDraw.picture(0.5, 0.2, "image/menu/BoutonRetour.png");
		}
		if(i==7) {
			StdDraw.picture(0.5, 0.5, "image/menu/wallpaper.jpg",1,1);
			StdDraw.picture(0.5, 0.75, "image/menu/logo.png",0.4,0.25);
			StdDraw.picture(0.5, 0.5, "image/menu/Crédit.gif");
			StdDraw.picture(0.5, 0.2, "image/menu/BoutonRetour.png");
		}
		
		
		if(i==0) {
			if (touche.hasFinished() && StdDraw.isMousePressed() && StdDraw.mouseX()>=0.4 && StdDraw.mouseX()<0.57
					&& StdDraw.mouseY()<0.6 && StdDraw.mouseY()>0.54) {			
				compte = compte(true);
				i=1;
				touche.restart();
			}
			if (touche.hasFinished() && StdDraw.isMousePressed() && StdDraw.mouseX()>=0.4 && StdDraw.mouseX()<0.57
					&& StdDraw.mouseY()<0.4 && StdDraw.mouseY()>0.34) {	
				compte = compte(false);
				i=1;
				touche.restart();
			}
		}
		
		if(i==1) {
			if (touche.hasFinished() && StdDraw.isMousePressed() && StdDraw.mouseX()>=0.4 && StdDraw.mouseX()<0.57
					&& StdDraw.mouseY()<0.6 && StdDraw.mouseY()>0.54) {			
				i=4;
				touche.restart();
			}
			if (touche.hasFinished() && StdDraw.isMousePressed() && StdDraw.mouseX()>=0.4 && StdDraw.mouseX()<0.57
					&& StdDraw.mouseY()<0.5 && StdDraw.mouseY()>0.44) {			
				i=2;
				touche.restart();
			}
			if (touche.hasFinished() && StdDraw.isMousePressed() && StdDraw.mouseX()>=0.4 && StdDraw.mouseX()<0.57
					&& StdDraw.mouseY()<0.4 && StdDraw.mouseY()>0.34) {			
				i=3;
				touche.restart();
			}
			if (touche.hasFinished() && StdDraw.isMousePressed() && StdDraw.mouseX()>=0.4 && StdDraw.mouseX()<0.57
					&& StdDraw.mouseY()<0.3 && StdDraw.mouseY()>0.24) {			
				System.exit(0);
				touche.restart();
			}
		}
		if(i==2) {
				if (touche.hasFinished() && StdDraw.isMousePressed() && StdDraw.mouseX()>=0.02 && StdDraw.mouseX()<0.19
						&& StdDraw.mouseY()<0.6 && StdDraw.mouseY()>0.54) {			
					if(compte.getNiveau()>=1) {
						niveau(compte, 1);
						niv = 1;
						menu=false;
					}
					if(compte.getNiveau() == 1) {
						bool=true;
					}
				}
				if (touche.hasFinished() && StdDraw.isMousePressed() && StdDraw.mouseX()>=0.22 && StdDraw.mouseX()<0.39
						&& StdDraw.mouseY()<0.6 && StdDraw.mouseY()>0.54) {			
					if(compte.getNiveau()>=2) {
						niveau(compte, 2);
						niv = 2;
						menu=false;
					}
					if(compte.getNiveau() == 2) {
						bool=true;
					}
				}
				if (touche.hasFinished() && StdDraw.isMousePressed() && StdDraw.mouseX()>=0.42 && StdDraw.mouseX()<0.59
						&& StdDraw.mouseY()<0.6 && StdDraw.mouseY()>0.54) {			
					if(compte.getNiveau()>=3) {
						niveau(compte, 3);
						niv = 3;
						menu=false;
					}
					if(compte.getNiveau() == 3) {
						bool=true;
					}
				}
				if (touche.hasFinished() && StdDraw.isMousePressed() && StdDraw.mouseX()>=0.62 && StdDraw.mouseX()<0.79
						&& StdDraw.mouseY()<0.6 && StdDraw.mouseY()>0.54) {			
					if(compte.getNiveau()>=4) {
						niveau(compte, 4);
						niv = 4;
						menu=false;
					}
					if(compte.getNiveau() == 4) {
						bool=true;
					}
				}
				if (touche.hasFinished() && StdDraw.isMousePressed() && StdDraw.mouseX()>=0.82 && StdDraw.mouseX()<0.99
						&& StdDraw.mouseY()<0.6 && StdDraw.mouseY()>0.54) {			
					if(compte.getNiveau()>=5) {
						niveau(compte, 5);
						niv = 5;
						menu=false;
					}
					if(compte.getNiveau() == 5) {
						bool=true;
					}
				}
				
				if (touche.hasFinished() && StdDraw.isMousePressed() && StdDraw.mouseX()>=0.02 && StdDraw.mouseX()<0.19
						&& StdDraw.mouseY()<0.5 && StdDraw.mouseY()>0.44) {			
					if(compte.getNiveau()>=6) {
						niveau(compte, 6);
						niv = 6;
						menu=false;
					}
					if(compte.getNiveau() == 6) {
						bool=true;
					}
				}
				if (touche.hasFinished() && StdDraw.isMousePressed() && StdDraw.mouseX()>=0.22 && StdDraw.mouseX()<0.39
						&& StdDraw.mouseY()<0.5 && StdDraw.mouseY()>0.44) {			
					if(compte.getNiveau()>=7) {
						niveau(compte, 7);
						niv = 7;
						menu=false;
					}
					if(compte.getNiveau() == 7) {
						bool=true;
					}
				}
				if (touche.hasFinished() && StdDraw.isMousePressed() && StdDraw.mouseX()>=0.42 && StdDraw.mouseX()<0.59
						&& StdDraw.mouseY()<0.5 && StdDraw.mouseY()>0.44) {			
					if(compte.getNiveau()>=8) {
						niveau(compte, 8);
						niv = 8;
						menu=false;
					}
					if(compte.getNiveau() == 8) {
						bool=true;
					}
				}
				if (touche.hasFinished() && StdDraw.isMousePressed() && StdDraw.mouseX()>=0.62 && StdDraw.mouseX()<0.79
						&& StdDraw.mouseY()<0.5 && StdDraw.mouseY()>0.44) {			
					if(compte.getNiveau()>=9) {
						niveau(compte, 9);
						niv = 9;
						menu=false;
					}
					if(compte.getNiveau() == 9) {
						bool=true;
					}
				}
				if (touche.hasFinished() && StdDraw.isMousePressed() && StdDraw.mouseX()>=0.82 && StdDraw.mouseX()<0.99
						&& StdDraw.mouseY()<0.5 && StdDraw.mouseY()>0.44) {			
					if(compte.getNiveau()>=10) {
						niveau(compte, 10);
						niv = 10;
						menu=false;
					}
					if(compte.getNiveau() == 10) {
						bool=true;
					}
				}
			if (touche.hasFinished() && StdDraw.isMousePressed() && StdDraw.mouseX()>=0.4 && StdDraw.mouseX()<0.57
					&& StdDraw.mouseY()<0.4 && StdDraw.mouseY()>0.34) {			
					menu=false;
					niveau(compte, 99);
					niv = 99;
					survie.debut();
			}
			if (touche.hasFinished() && StdDraw.isMousePressed() && StdDraw.mouseX()>=0.4 && StdDraw.mouseX()<0.57
					&& StdDraw.mouseY()<0.3 && StdDraw.mouseY()>0.24) {			
				i=1;
				touche.restart();
			}
		}
		if(i==3) {
			if (touche.hasFinished() && StdDraw.isMousePressed() && StdDraw.mouseX()>=0.4 && StdDraw.mouseX()<0.57
					&& StdDraw.mouseY()<0.3 && StdDraw.mouseY()>0.24) {			
				i=1;
				touche.restart();
			}
		}
		if(i==4) {
			if (touche.hasFinished() && StdDraw.isMousePressed() && StdDraw.mouseX()>=0.4 && StdDraw.mouseX()<0.57
					&& StdDraw.mouseY()<0.3 && StdDraw.mouseY()>0.24) {			
				i=1;
				touche.restart();
			}
		}
		if(i==5) {
			if (touche.hasFinished() && StdDraw.isMousePressed() && StdDraw.mouseX()>=0.4 && StdDraw.mouseX()<0.57
					&& StdDraw.mouseY()<0.5 && StdDraw.mouseY()>0.44) {	
					niveau(compte, niv);
					menu=false;
					if(compte.getNiveau()==niv)bool=true;
					niv++;
					GameWorld.niveau++;
			}
			if (touche.hasFinished() && StdDraw.isMousePressed() && StdDraw.mouseX()>=0.4 && StdDraw.mouseX()<0.57
					&& StdDraw.mouseY()<0.3 && StdDraw.mouseY()>0.24) {			
				i=2;
				touche.restart();
			}
		}
		if(i==6) {
			if (touche.hasFinished() && StdDraw.isMousePressed() && StdDraw.mouseX()>=0.4 && StdDraw.mouseX()<0.57
					&& StdDraw.mouseY()<0.5 && StdDraw.mouseY()>0.44) {	
					niveau(compte, niv);
					menu=false;
					bool=false;
			}
			if (touche.hasFinished() && StdDraw.isMousePressed() && StdDraw.mouseX()>=0.4 && StdDraw.mouseX()<0.57
					&& StdDraw.mouseY()<0.3 && StdDraw.mouseY()>0.24) {			
				i=2;
				touche.restart();
			}
		}
		if(i==7) {
			if (touche.hasFinished() && StdDraw.isMousePressed() && StdDraw.mouseX()>=0.4 && StdDraw.mouseX()<0.57
					&& StdDraw.mouseY()<0.3 && StdDraw.mouseY()>0.24) {			
				System.exit(0);
				touche.restart();
			}
		}
		
		
	}

	/**
	 * On active les musiques pour différentes situations
	 */
	public static void MusicNiveau1() {
		if(replay1.hasFinished()) {
			music = true;
			replay1.restart();
		}
			if(music) {
				replay1.restart();
				try {
					musique = new Sound("Musics/MusicNiveau1.mp3");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				
				run1 = new Runner();
				stop1 = new Stopper();
				run1.start();
				music =false;
			}
	}

	public static void MusicNiveau2() {
		if(replay2.hasFinished()) {
			music = true;
			replay2.restart();
		}
			if(music) {
				replay2.restart();
				try {
					musique = new Sound("Musics/MusicNiveau2.mp3");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				run2 = new Runner();
				stop2 = new Stopper();
				run2.start();
				music =false;
			}
	}

	public static void MusicNiveau3() {
		if(replay3.hasFinished()) {
			music = true;
			replay3.restart();
		}
			if(music) {
				replay3.restart();
				try {
					musique = new Sound("Musics/MusicNiveau3.mp3");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				run3 = new Runner();
				stop3 = new Stopper();
				run3.start();
				music =false;
			}
	}

	public static void MusicNiveau4() {
		if(replay4.hasFinished()) {
			music = true;
			replay4.restart();
		}
			if(music) {
				replay4.restart();
				try {
					musique = new Sound("Musics/MusicNiveau4.mp3");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				run4 = new Runner();
				stop4 = new Stopper();
				run4.start();
				music =false;
			}
	}

	public static void MusicNiveau5() {
		if(replay5.hasFinished()) {
			music = true;
			replay5.restart();
		}
			if(music) {
				replay5.restart();
				try {
					musique = new Sound("Musics/MusicNiveau5.mp3");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				run5 = new Runner();
				stop5 = new Stopper();
				run5.start();
				music =false;
			}
	}

	public static void MusicNiveau6() {
		if(replay6.hasFinished()) {
			music = true;
			replay6.restart();
		}
			if(music) {
				replay6.restart();
				try {
					musique = new Sound("Musics/MusicNiveau6.mp3");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				run6 = new Runner();
				stop6 = new Stopper();
				run6.start();
				music =false;
			}
	}

	public static void MusicNiveau7() {
		if(replay7.hasFinished()) {
			music = true;
			replay7.restart();
		}
			if(music) {
				replay7.restart();
				try {
					musique = new Sound("Musics/MusicNiveau7.mp3");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				run7 = new Runner();
				stop7 = new Stopper();
				run7.start();
				music =false;
			}
	}

	public static void MusicNiveau8() {
		if(replay8.hasFinished()) {
			music = true;
			replay8.restart();
		}
			if(music) {
				replay8.restart();
				try {
					musique = new Sound("Musics/MusicNiveau8.mp3");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				run8 = new Runner();
				stop8 = new Stopper();
				run8.start();
				music =false;
			}
	}

	public static void MusicNiveau9() {
		if(replay9.hasFinished()) {
			music = true;
			replay9.restart();
		}
			if(music) {
				replay9.restart();
				try {
					musique = new Sound("Musics/MusicNiveau9.mp3");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				run9 = new Runner();
				stop9 = new Stopper();
				run9.start();
				music =false;
			}
	}

	public static void MusicNiveau10() {
		if(replay10.hasFinished()) {
			music = true;
			replay10.restart();
		}
			if(music) {
				replay10.restart();
				try {
					musique = new Sound("Musics/MusicNiveau10.mp3");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				run10 = new Runner();
				stop10 = new Stopper();
				run10.start();
				music =false;
			}
	}
	public static void MusicSurvie() {
		if(replayS.hasFinished()) {
			music = true;
			replayS.restart();
		}
			if(music) {
				replayS.restart();
				try {
					musique = new Sound("Musics/Survie.mp3");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				runS = new Runner();
				stopS = new Stopper();
				runS.start();
				music =false;
			}
	}
	public static void MusicCredit() {
		if(replayC.hasFinished()) {
			music = true;
			replayC.restart();
		}
			if(music) {
				replayC.restart();
				try {
					musique = new Sound("Musics/MusicCredit.mp3");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				runC = new Runner();
				stopC = new Stopper();
				runC.start();
				music =false;
			}
	}

	public static void MusicGameWon() {
			if(music) {
				try {
					musique = new Sound("Musics/GameWon.mp3");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				runW = new Runner();
				stopW = new Stopper();
				runW.start();
				music =false;
			}
	}
	public static void MusicGameLost() {
			if(music) {
				try {
					musique = new Sound("Musics/GameLost.mp3");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				runL = new Runner();
				stopL = new Stopper();
				runL.start();
				music =false;
			}
	}
	public static void MusicMenu() {
		if(replayMenu.hasFinished()) {
			music = true;
			replayMenu.restart();
		}
			if(music) {
				replayMenu.restart();
				try {
					musique = new Sound("Musics/Menu.mp3");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				runMenu = new Runner();
				stopMenu = new Stopper();
				runMenu.start();
				music =false;
			}
	}

	
	
	public static void main(String[] args) {
		
		// reglage de la taille de la fenetre de jeu, en pixels (nb: un écran fullHD = 1980x1050 pixels)
			StdDraw.setCanvasSize(1000, 1000);
			
		// permet le double buffering, pour permettre l'animation
			StdDraw.enableDoubleBuffering();		
	
		//le menu
		while(menu) {
			StdDraw.clear();
			
			Menu();
			
			MusicMenu();
			
			if(!menu) {
				stopMenu.start();
				music = true;
			}
			StdDraw.show();
			StdDraw.pause(20);
		}
		
		GameWorld world = new GameWorld();
		
		// la boucle principale de notre jeu
		while (!GameWorld.gameWon()) {
			// lancement des musiques
			if (niv == 1) {
				MusicNiveau1();
			}
			if (niv == 2) {
				MusicNiveau2();
			}
			if (niv == 3) {
				MusicNiveau3();
			}
			if (niv == 4) {
				MusicNiveau4();
			}
			if (niv == 5) {
				MusicNiveau5();
			}
			if (niv == 6) {
				MusicNiveau6();
			}
			if (niv == 7) {
				MusicNiveau7();
			}
			if (niv == 8) {
				MusicNiveau8();
			}
			if (niv == 9) {
				MusicNiveau9();
			}
			if (niv == 10) {
				MusicNiveau10();
			}
			if (niv == 99) {
				MusicSurvie();
			}
			
			if (GameWorld.levelWon()) {
				
				if (niv == 1) {
					stop1.start();
				}
				if (niv == 2) {
					stop2.start();
				}
				if (niv == 3) {
					stop3.start();
				}
				if (niv == 4) {
					stop4.start();
				}
				if (niv == 5) {
					stop5.start();
				}
				if (niv == 6) {
					stop6.start();
				}
				if (niv == 7) {
					stop7.start();
				}
				if (niv == 8) {
					stop8.start();
				}
				if (niv == 9) {
					stop9.start();
				}
				if (niv == 99) {
					stopS.start();
						survie.fin();
						int s = (int) survie.time();
						compte.setScore(Integer.toString(s));
						
						String[] t = new String[2];
						t[0] = str;
						t[1] = Integer.toString(s);
						classement.setClassement(t);
				}
				music = true;
				compte.niveauSup(bool);
				menu = true;
				touche.restart();
				i=5;
				
				//le menu
				while(menu) {
					StdDraw.clear();
					
					Menu();
					
					if(i==5) {
						MusicGameWon();
						replayMenu.restart();
					}
					if((!menu || i!=5) && !transition) {
						stopW.start();
						music = true;
						transition = true;
					}
					
					MusicMenu();
					
					if(!menu) {
						stopMenu.start();
						music = true;
						transition = false;
					}
					StdDraw.show();
					StdDraw.pause(20);
				}
				
				GameWorld.levelWon = false;
				GameWorld.entites.clear();
				Level.depart1 = false;
				Level.depart2 = false;
				Level.depart3 = false;
				Level.depart4 = false;
				Level.depart5 = false;
				Level.depart6 = false;
				Level.depart7 = false;
				Level.depart8 = false;
				Level.depart9 = false;
				Level.depart10 = false;
				Level.tond = true;
				Grille.clear();
				GrilleSun.clear();
				GameWorld.totalSun = 50;
				GameWorld.imageDebut = new Timer(20000);
				Level.debut = new Timer(20000);
				GameWorld.triche = false;
				GameWorld.vitesse = false;
				GameWorld.tpstext.restart();
				StdDraw.clear();
			}
			
			if (GameWorld.gameLost()) {
					
					
				if (niv == 1) {
					stop1.start();
				}
				if (niv == 2) {
					stop2.start();
				}
				if (niv == 3) {
					stop3.start();
				}
				if (niv == 4) {
					stop4.start();
				}
				if (niv == 5) {
					stop5.start();
				}
				if (niv == 6) {
					stop6.start();
				}
				if (niv == 7) {
					stop7.start();
				}
				if (niv == 8) {
					stop8.start();
				}
				if (niv == 9) {
					stop9.start();
				}
				if (niv == 10) {
					stop10.start();
				}
				if (niv == 99) {
					stopS.start();
						survie.fin();
						int s = (int) survie.time();
						compte.setScore(Integer.toString(s));
						
						String[] t = new String[2];
						t[0] = str;
						t[1] = Integer.toString(s);
						classement.setClassement(t);
				}
				music = true;
				menu = true;

				
				GameWorld.gameLost = false;	
				touche.restart();
				i=6;
				
				//le menu
				while(menu) {
					StdDraw.clear();
					
					Menu();
					
					if(i==6) {
						MusicGameLost();
						replayMenu.restart();
					}
					if((!menu || i!=6) && !transition) {
						stopL.start();
						music = true;
						transition = true;
					}
					
					MusicMenu();
					
					if(!menu) {
						stopMenu.start();
						music = true;
						transition = false;
					}
					StdDraw.show();
					StdDraw.pause(20);
				}
				
				GameWorld.entites.clear();
				Level.depart1 = false;
				Level.depart2 = false;
				Level.depart3 = false;
				Level.depart4 = false;
				Level.depart5 = false;
				Level.depart6 = false;
				Level.depart7 = false;
				Level.depart8 = false;
				Level.depart9 = false;
				Level.depart10 = false;
				Level.tond = true;
				Grille.clear();
				GrilleSun.clear();
				GameWorld.totalSun = 50;
				GameWorld.imageDebut = new Timer(20000);
				Level.debut = new Timer(20000);
				GameWorld.triche = false;
				GameWorld.vitesse = false;
				GameWorld.tpstext.restart();
				
			StdDraw.clear();
		}
			// Efface la fenetre graphique
			StdDraw.clear();
			
			// Capture et traite les eventuelles interactions clavier du joueur
			if (StdDraw.hasNextKeyTyped()) {
				char key = StdDraw.nextKeyTyped();
				world.processUserInput(key);
			}
			
			if (StdDraw.isMousePressed()) {
				world.processMouseClick(StdDraw.mouseX(), StdDraw.mouseY());
			}
			
			world.step();
			
			
			// dessine la carte
			world.dessine();
			
			world.processMouse(StdDraw.mouseX(), StdDraw.mouseY());
			// Montre la fenetre graphique mise a jour et attends 20 millisecondes
			StdDraw.show();
			StdDraw.pause(20);
			
			
		}
		
		if (GameWorld.gameWon()) {
			stop10.start();
			music = true;
			menu = true;
			touche.restart();
			i=7;
			//le menu
			while(menu) {
				StdDraw.clear();
				
				Menu();
				
				MusicCredit();
				
				StdDraw.show();
				StdDraw.pause(20);
			}
			
			StdDraw.clear();
		}

	}

}
