
public class Pois extends Entite{

	private int degats;
	private int ID;
	
	private static final double TRUC_MOVE_X = 0.00152; // 0.5 case par secondes
	private static final double TRUC_SIZE = 0.1;
	private boolean statutTir;
	
	public Pois(double x, double y, int id) {
		super(x, y);
		this.ID = id;
		this.degats = 20;
		this.statutTir = true;
		// TODO Auto-generated constructor stub
	}
	
	
	public int getID() {
		return ID;
	}



	public void setID(int iD) {
		ID = iD;
	}



	public int getDegats() {
		return degats;
	}



	public void setDegats(int degats) {
		this.degats = degats;
	}

	@Override
	public void step() {
		// TODO Auto-generated method stub
		if (this.position.getX() > 0.76) {
			statutTir = false; 
		}else {
			if(GameWorld.grille.collisionZombie(this.position.getX(), this.position.getY())) {
				statutTir = false;
				if(GameWorld.grille.tapeZombie(this.position.getX(), this.position.getY()) instanceof ZombieBase){
					((ZombieBase) GameWorld.entites.get(GameWorld.grille.tapeZombie(this.position.getX(), this.position.getY()).getID())).setVie(((ZombieBase) GameWorld.entites.get(GameWorld.grille.tapeZombie(this.position.getX(), this.position.getY()).getID())).getVie() - getDegats());
					((ZombieBase) GameWorld.grille.tapeZombie(this.position.getX(), this.position.getY())).setVie(((ZombieBase) GameWorld.grille.tapeZombie(this.position.getX(), this.position.getY())).getVie() - getDegats());
				}else if(GameWorld.grille.tapeZombie(this.position.getX(), this.position.getY()) instanceof ZombieBlinde) {
					((ZombieBlinde) GameWorld.entites.get(GameWorld.grille.tapeZombie(this.position.getX(), this.position.getY()).getID())).setVie(((ZombieBlinde) GameWorld.entites.get(GameWorld.grille.tapeZombie(this.position.getX(), this.position.getY()).getID())).getVie() - getDegats());
					((ZombieBlinde) GameWorld.grille.tapeZombie(this.position.getX(), this.position.getY())).setVie(((ZombieBlinde) GameWorld.grille.tapeZombie(this.position.getX(), this.position.getY())).getVie() - getDegats());
				}else if(GameWorld.grille.tapeZombie(this.position.getX(), this.position.getY()) instanceof ZombieBoss) {
					((ZombieBoss) GameWorld.entites.get(GameWorld.grille.tapeZombie(this.position.getX(), this.position.getY()).getID())).setVie(((ZombieBoss) GameWorld.entites.get(GameWorld.grille.tapeZombie(this.position.getX(), this.position.getY()).getID())).getVie() - getDegats());
					((ZombieBoss) GameWorld.grille.tapeZombie(this.position.getX(), this.position.getY())).setVie(((ZombieBoss) GameWorld.grille.tapeZombie(this.position.getX(), this.position.getY())).getVie() - getDegats());
				}
			}else {
				this.position.setX(this.position.getX() + TRUC_MOVE_X);
			}
		}
		
		if (!statutTir) {
			GameWorld.entites.set(getID(), null);
		}
	}

	@Override
	public void dessine() {
		// TODO Auto-generated method stub
		if (statutTir) {
			StdDraw.picture(this.position.getX()+0.02, this.position.getY()-0.05, "image/pois.png", TRUC_SIZE, TRUC_SIZE);
		}
		
		
	}

}
