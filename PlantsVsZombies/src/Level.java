import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Level {

	//le timer pour savoir quand on commence
	static Timer debut;
	
	static Timer wait;
	static Timer debut2;
	//un boolean pour savoir si le debut du niveau est lancé
	static boolean depart1;
	static boolean depart2;
	static boolean depart3;
	static boolean depart4;
	static boolean depart5;
	static boolean depart6;
	static boolean depart7;
	static boolean depart8;
	static boolean depart9;
	static boolean depart10;
	
	//des variables pour les positions aléatoires des zombies
	private List<Double> t;
	private Random rand;
	
	//le timer d'apparition du soleil
	private Timer sun;
	
	static boolean tond;
	
	public Level() {
		//on itialise les variables
		debut = new Timer(20000);//timer de 20s
		debut2 = new Timer(2000);
		wait = new Timer(2000);
		
		depart1 = false;
		depart2 = false;
		depart3 = false;
		depart4 = false;
		depart5 = false;
		depart6 = false;
		depart7 = false;
		depart8 = false;
		depart9 = false;
		depart10 = false;
		
		t = new LinkedList<Double>();
		rand = new Random();
		
		sun = new Timer(0);
		
		tond = true;
	}
	
	/**
	 * rend un double entre 1.0 et 3.0
	 * 
	 * @return x un double compris entre 1.0 et 3.0
	 * 
	 */
	public double hasardX() {
		double x = 1 + rand.nextInt(1) + rand.nextDouble();
		return x;
	}
	
	/**
	 * rend un double entre 0.3 et 0.75
	 * utilise une liste pour que les y soit différents pour 20 chaque zombies 
	 * 
	 * @return y un double compris entre 0.3 et 0.75
	 * 
	 */
	public double hasardY() {
		if(t.size()>20) {
			t.clear();
		}
		double y = rand.nextDouble();
		if(y<0.3) {
			y += 0.3 ;
		}else if(y>0.75) {
			y -= 0.25;
		}
		if(!(t.contains(y))) {
			t.add(y);
			return y;
		}else {
			return hasardY();
		}
	}
	
	/**
	 * rend un double entre 0.07 et 0.75
	 * 
	 * @return x un double compris entre 0.07 et 0.75
	 * 
	 */
	public double hasardXsun() {
		double x = rand.nextDouble();
		if(x<0.07) {
			x += 0.07 ;
		}else if(x>0.75) {
			x -= 0.25;
		}
		return x;
	}
	
	/**
	 * sélectionne le niveau désiré
	 * 
	 * @param int le numero du niveau
	 * 
	 */
	public void launch(int lvl) {
		if(lvl == 1) {
			level1();
		}else if(lvl == 2) {
			level2();
		}else if(lvl == 3) {
			level3();
		}else if(lvl == 4) {
			level4();
		}else if(lvl == 5) {
			level5();
		}else if(lvl == 6) {
			level6();
		}else if(lvl == 7) {
			level7();
		}else if(lvl == 8) {
			level8();
		}else if(lvl == 9) {
			level9();
		}else if(lvl == 10) {
			level10();
		}
		if(lvl== 99) {
			survie();
		}
	}
	
	/**
	 * ajoute tous les zombies au jeu avec une difficulté croissante 
	 * et le soleil
	 * 
	 */
	private void level1() {
		if(sun.hasFinished()) {
			sun = new Timer(6500);
			Soleil p = new Soleil(hasardXsun(), hasardY(), GameWorld.entites.size(), true);
			GameWorld.ajoute(p);
			GameWorld.grilleSun.ajoute(p, p.position.getX(), p.position.getY());
		}
		if(debut.hasFinished() && !depart1) {
			GameWorld.ajoute(new ZombieBase(0.8,0.5,GameWorld.entites.size()));
			GameWorld.ajoute(new ZombieBase(1.2,0.4,GameWorld.entites.size()));
			GameWorld.ajoute(new ZombieBase(1.3,0.65,GameWorld.entites.size()));
			GameWorld.ajoute(new ZombieBlinde(1.5, hasardY(),GameWorld.entites.size()));
			ZombieBoss z = new ZombieBoss(3.0,0.5,GameWorld.entites.size());
			z.setVie(500);
			GameWorld.ajoute(z);
			
			depart1= true;
		}
	}
	
	/**
	 * ajoute tous les zombies au jeu avec une difficulté croissante 
	 * et le soleil
	 * 
	 */
	private void level2() {
		if(tond) {
			Tondeuse t1 = new Tondeuse(0.06,0.3,GameWorld.entites.size());
			GameWorld.ajoute(t1);
			Tondeuse t2 = new Tondeuse(0.06,0.4,GameWorld.entites.size());
			GameWorld.ajoute(t2);
			Tondeuse t3 = new Tondeuse(0.06,0.5,GameWorld.entites.size());
			GameWorld.ajoute(t3);
			Tondeuse t4 = new Tondeuse(0.06,0.6,GameWorld.entites.size());
			GameWorld.ajoute(t4);
			Tondeuse t5 = new Tondeuse(0.06,0.7,GameWorld.entites.size());
			GameWorld.ajoute(t5);
			tond = false;
		}
		
		level1();
		int z = 0;
		for(int i=0;i<GameWorld.entites.size();i++) {
			if(GameWorld.entites.get(i) instanceof Zombie) {
				if(GameWorld.entites.get(i).getX()>0.76) {
					z++;
				}
			}
		}
		if(debut.hasFinished() && !depart2 && z==0) {
			
			GameWorld.ajoute(new ZombieBoss(4.0,0.5,GameWorld.entites.size()));
			for(int i=0;i<10;i++) {
				if(i>7) {
					GameWorld.ajoute(new ZombieBlinde(1+hasardX(),hasardY(),GameWorld.entites.size()));
				}
				
				GameWorld.ajoute(new ZombieBase(hasardX(),hasardY(),GameWorld.entites.size()));
			}
			
			depart2= true;
		}
	}
	
	
	private void level3() {
		level2();
		int z = 0;
		for(int i=0;i<GameWorld.entites.size();i++) {
			if(GameWorld.entites.get(i) instanceof Zombie) {
				if(GameWorld.entites.get(i).getX()>0.76) {
					z++;
				}
			}
		}
		if(debut.hasFinished() && !depart3 && z==0) {
				GameWorld.ajoute(new ZombieBoss(4.0,0.5,GameWorld.entites.size()));
				for(int i=0;i<10;i++) {
					if(i>7) {
						GameWorld.ajoute(new ZombieBlinde(1+hasardX(),hasardY(),GameWorld.entites.size()));
						GameWorld.ajoute(new ZombieBlinde(1+hasardX(),hasardY(),GameWorld.entites.size()));
					}
					
					GameWorld.ajoute(new ZombieBase(hasardX(),hasardY(),GameWorld.entites.size()));
				}
				
				depart3= true;
			}
		
	}
	
	private void level4() {
		level3();
		int z = 0;
		for(int i=0;i<GameWorld.entites.size();i++) {
			if(GameWorld.entites.get(i) instanceof Zombie) {
				if(GameWorld.entites.get(i).getX()>0.76) {
					z++;
				}
			}
		}
		if(debut.hasFinished() && !depart4 && z==0) {
			GameWorld.ajoute(new ZombieBoss(5.0,0.5,GameWorld.entites.size()));
			for(int i=0;i<15;i++) {
				if(i>12) {
					GameWorld.ajoute(new ZombieBlinde(1+hasardX(),hasardY(),GameWorld.entites.size()));
					GameWorld.ajoute(new ZombieBlinde(1+hasardX(),hasardY(),GameWorld.entites.size()));
				}
				
				GameWorld.ajoute(new ZombieBase(hasardX(),hasardY(),GameWorld.entites.size()));
			}
			
			depart4= true;
		}
	}
	
	private void level5() {
		level4();
		int z = 0;
		for(int i=0;i<GameWorld.entites.size();i++) {
			if(GameWorld.entites.get(i) instanceof Zombie) {
				if(GameWorld.entites.get(i).getX()>0.76) {
					z++;
				}
			}
		}
		if(debut.hasFinished() && !depart5 && z==0) {
			GameWorld.ajoute(new ZombieBoss(5.0,0.5,GameWorld.entites.size()));
			for(int i=0;i<20;i++) {
				if(i>15) {
					GameWorld.ajoute(new ZombieBlinde(1+hasardX(),hasardY(),GameWorld.entites.size()));
					GameWorld.ajoute(new ZombieBlinde(1+hasardX(),hasardY(),GameWorld.entites.size()));
				}
				
				GameWorld.ajoute(new ZombieBase(hasardX(),hasardY(),GameWorld.entites.size()));
			}
			
			depart5= true;
		}
	}
	
	private void level6() {
		level5();
		int z = 0;
		for(int i=0;i<GameWorld.entites.size();i++) {
			if(GameWorld.entites.get(i) instanceof Zombie) {
				if(GameWorld.entites.get(i).getX()>0.76) {
					z++;
				}
			}
		}
		if(debut.hasFinished() && !depart6 && z==0) {
			GameWorld.ajoute(new ZombieBoss(5.0,0.5,GameWorld.entites.size()));
			for(int i=0;i<20;i++) {
				if(i>15) {
					GameWorld.ajoute(new ZombieBlinde(1+hasardX(),hasardY(),GameWorld.entites.size()));
					GameWorld.ajoute(new ZombieBlinde(1+hasardX(),hasardY(),GameWorld.entites.size()));
					GameWorld.ajoute(new ZombieBlinde(1+hasardX(),hasardY(),GameWorld.entites.size()));
				}
				
				GameWorld.ajoute(new ZombieBase(hasardX(),hasardY(),GameWorld.entites.size()));
			}
			
			depart6= true;
		}
	}
	
	private void level7() {
		level6();
		int z = 0;
		for(int i=0;i<GameWorld.entites.size();i++) {
			if(GameWorld.entites.get(i) instanceof Zombie) {
				if(GameWorld.entites.get(i).getX()>0.76) {
					z++;
				}
			}
		}
		if(debut.hasFinished() && !depart7 && z==0) {
			GameWorld.ajoute(new ZombieBoss(5.0,0.5,GameWorld.entites.size()));
			for(int i=0;i<25;i++) {
				if(i>15) {
					GameWorld.ajoute(new ZombieBlinde(1+hasardX(),hasardY(),GameWorld.entites.size()));
					GameWorld.ajoute(new ZombieBlinde(1+hasardX(),hasardY(),GameWorld.entites.size()));
				}
				
				GameWorld.ajoute(new ZombieBase(hasardX(),hasardY(),GameWorld.entites.size()));
			}
			
			depart7= true;
		}
	}
	
	private void level8() {
		level7();
		int z = 0;
		for(int i=0;i<GameWorld.entites.size();i++) {
			if(GameWorld.entites.get(i) instanceof Zombie) {
				if(GameWorld.entites.get(i).getX()>0.76) {
					z++;
				}
			}
		}
		if(debut.hasFinished() && !depart8 && z==0) {
			GameWorld.ajoute(new ZombieBoss(5.0,0.5,GameWorld.entites.size()));
			for(int i=0;i<25;i++) {
				if(i>15) {
					GameWorld.ajoute(new ZombieBlinde(1+hasardX(),hasardY(),GameWorld.entites.size()));
					GameWorld.ajoute(new ZombieBlinde(1+hasardX(),hasardY(),GameWorld.entites.size()));
					GameWorld.ajoute(new ZombieBlinde(1+hasardX(),hasardY(),GameWorld.entites.size()));
				}
				
				GameWorld.ajoute(new ZombieBase(hasardX(),hasardY(),GameWorld.entites.size()));
			}
			
			depart8= true;
		}
	}
	
	private void level9() {
		level8();
		int z = 0;
		for(int i=0;i<GameWorld.entites.size();i++) {
			if(GameWorld.entites.get(i) instanceof Zombie) {
				if(GameWorld.entites.get(i).getX()>0.76) {
					z++;
				}
			}
		}
		if(debut.hasFinished() && !depart9 && z==0) {
			GameWorld.ajoute(new ZombieBoss(5.0,0.5,GameWorld.entites.size()));
			for(int i=0;i<30;i++) {
				if(i>20) {
					GameWorld.ajoute(new ZombieBlinde(1+hasardX(),hasardY(),GameWorld.entites.size()));
					GameWorld.ajoute(new ZombieBlinde(1+hasardX(),hasardY(),GameWorld.entites.size()));
				}
				
				GameWorld.ajoute(new ZombieBase(hasardX(),hasardY(),GameWorld.entites.size()));
			}
			
			depart9= true;
		}
	}
	
	private void level10() {
		level9();
		int z = 0;
		for(int i=0;i<GameWorld.entites.size();i++) {
			if(GameWorld.entites.get(i) instanceof Zombie) {
				if(GameWorld.entites.get(i).getX()>0.76) {
					z++;
				}
			}
		}
		if(debut.hasFinished() && !depart10 && z==0) {
			GameWorld.ajoute(new ZombieBossPaul(5.0,0.5,GameWorld.entites.size()));
			for(int i=0;i<30;i++) {
				if(i>20) {
					GameWorld.ajoute(new ZombieBlinde(1+hasardX(),hasardY(),GameWorld.entites.size()));
					GameWorld.ajoute(new ZombieBlinde(1+hasardX(),hasardY(),GameWorld.entites.size()));
					GameWorld.ajoute(new ZombieBlinde(1+hasardX(),hasardY(),GameWorld.entites.size()));
				}
				
				GameWorld.ajoute(new ZombieBase(hasardX(),hasardY(),GameWorld.entites.size()));
			}
			
			depart10= true;
		}
	}
	
	private void survie() {
		level10();
		int z = 0;
		for(int i=0;i<GameWorld.entites.size();i++) {
			if(GameWorld.entites.get(i) instanceof Zombie) {
				if(GameWorld.entites.get(i).getX()>0.76) {
					z++;
				}
			}
		}
		if(debut.hasFinished() && z==0) {
			
			for(int i=0;i<30;i++) {
				if(i>20) {
					GameWorld.ajoute(new ZombieBlinde(hasardX(),hasardY(),GameWorld.entites.size()));
					GameWorld.ajoute(new ZombieBlinde(hasardX(),hasardY(),GameWorld.entites.size()));
					GameWorld.ajoute(new ZombieBlinde(hasardX(),hasardY(),GameWorld.entites.size()));
				}
				
				GameWorld.ajoute(new ZombieBase(hasardX(),hasardY(),GameWorld.entites.size()));
			}
			GameWorld.ajoute(new ZombieBoss(3.0,0.5,GameWorld.entites.size()));
		}
	}
	
	/**
	 * si un zombie atteint la maison le jeu est perdu 
	 * 
	 * @return boolean true si le jeu est perdu
	 * 
	 */
	public boolean perdu() {
		for(int i=0;i<GameWorld.entites.size();i++) {
			if(GameWorld.entites.get(i) instanceof Zombie) {
				if(GameWorld.entites.get(i).position.getX() < 0.05) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * si tous les zombies sont morts c'est gagné
	 * 
	 * @return boolean true si il n'y a plus de zombie
	 * 
	 */
	public boolean gagne() {
		int z=0;
		if(!debut.hasFinished()) {
			debut2.restart();
		}
		if(debut2.hasFinished()) {
			for(int i=0;i<GameWorld.entites.size();i++) {
				if(GameWorld.entites.get(i) instanceof Zombie) {
					z++;
				}
			}
			if(z==0) {
				return true;
			}
		}
		return false;
	}
	
	
}
